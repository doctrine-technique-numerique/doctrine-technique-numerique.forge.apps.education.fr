# Doctrine technique du numérique pour l'éducation

Le ministère chargé de l’Éducation nationale publie la « doctrine technique du numérique pour l’éducation », afin de mettre en place un cadre d’architecture et de règles communes, visant à fournir aux usagers un ensemble lisible et structuré de services numériques éducatifs accessibles simplement et interopérables.

## Pourquoi une doctrine technique du numérique pour l’éducation ?

L’utilisation des services numériques éducatifs dans les 1er et 2d degrés a connu une croissance accélérée depuis 2020. Les usages sont toujours plus nombreux, non seulement à des fins administratives et de suivi de la scolarité, mais aussi au bénéfice d’activités pédagogiques et de mise en œuvre des missions éducatives. Le corollaire est logiquement une augmentation conséquente de la fréquentation des services, qui nécessite à la fois une parfaite maîtrise des données ainsi qu’un écosystème ouvert et interopérable. Avec des services numériques au cœur de la continuité pédagogique, les attentes et les besoins associés sont nombreux sur la simplification des parcours usagers, la disponibilité des services, le cadre de confiance qui assure la sécurité des données et leur échange entre des acteurs habilités.

## Objectifs et positionnement de la doctrine technique du numérique pour l’éducation

Afin de mettre à disposition des usagers un ensemble lisible et structuré de services numériques accessibles simplement et interopérables entre eux, le numérique pour l’éducation doit s’organiser selon une logique de « plateforme ». Pour cela, un cadre d’architecture et des règles communes, facilitant la circulation des données entre les acteurs publics et privés, sont rassemblés dans un document : la doctrine technique du numérique pour l’éducation. Elle est publiée ici dans sa première version.

La doctrine technique exprime les exigences attendues pour les fournisseurs de services numériques éducatifs et renvoie à 3 référentiels en cours d’élaboration, dédiés aux exigences d’interopérabilité, de sécurité, de numérique responsable et qui ont vocation à devenir opposables par voie législative. Cet ensemble documentaire est issu d’un travail mené en collaboration avec les parties prenantes concernées.

## Documents de référence

Doctrine technique du numérique pour l’éducation : 
- [version web](https://doctrine-technique-numerique.forge.apps.education.fr/)
- [version en PDF](https://eduscol.education.fr/document/49067/download?attachment)

## Site web de la doctrine technique du numérique pour l'éducation

Ce site utilise le thème [mkdocs-dsfr](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/mkdocs-dsfr), un thème mkdocs conforme aux standards de l'État français pour la création de sites Web.

- Voir cet exemple sur [la page Gitlab](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/numeco/mkdocs-dsfr-exemple)

### Prérequis

- [Python 3.x](https://www.python.org/downloads/)
- [pipenv](https://pipenv.pypa.io/en/latest/)

### Installation

1. Clonez ce dépôt sur votre machine locale.
2. Ouvrez un terminal et naviguez vers le dossier du projet.
3. Exécutez `pipenv install` pour installer les dépendances du projet.

###  Utilisation

#### Environnement de Développement

1. Entrez dans l'environnement virtuel avec `pipenv shell`.
2. Lancez le serveur de développement avec `mkdocs serve`.
3. Ouvrez votre navigateur Web et accédez à `http://127.0.0.1:8000`.

#### Construction du Site

Pour construire le site Web statique, utilisez la commande suivante :

```bash
mkdocs build
```

Le site sera généré dans le dossier `site`.

### Ressources Utiles

- [Site officiel de DSFR](https://www.systeme-de-design.gouv.fr/)
- [Documentation mkdocs](https://www.mkdocs.org/)