# 2. Dictionnaire des notions et processus métiers

Pour définir les exigences qui concernent chaque acteur, les notions métier doivent être précisées afin de partager les mêmes objets et clarifier les finalités (y compris pour les formalités RGPD).

## 2.1. Organisation pédagogique

L’organisation pédagogique désigne l’ensemble des principes et des règles régissant d’une part l'organisation et la façon dont un établissement scolaire ou une école gère ses moyens en ressources humaines et matérielles et d’autre part les méthodes et ressources adoptées par les enseignants pour enseigner.

Dans le second degré, l’organisation pédagogique de l’établissement (services d’enseignement, services d’évaluation, répartition des élèves, emploi du temps…) incombe au chef d’établissement.

Dans le premier degré, il revient au directeur d'école, dans le cadre du projet d'école, d'assurer la coordination nécessaire entre les maîtres, d'animer l'équipe pédagogique et de veiller au bon déroulement des enseignements[^1].

En pratique, la plupart des processus et activités s’appuyant sur l’organisation pédagogique définie par le chef d’établissement sont réalisés à l’aide de solutions numériques. Par exemple, les logiciels dits « d’emploi du temps » permettent de matérialiser et de mettre à jour l’organisation pédagogique optimisée dans l’espace et le temps, c’est-à-dire l’emploi du temps. L’organisation pédagogique définie alimente également les logiciels dits de « vie scolaire » qui outillent notamment le suivi de l’assiduité des élèves (absences, évaluations…), telle que définie au code de l’éducation. 

Les espaces numériques de travail, les systèmes d’information d’évaluation ou de certification s’appuient eux aussi sur cette organisation. Sa circulation fluide sans ressaisie au passage d’un outil à l’autre contribue à simplifier les tâches et à faciliter les usages.

Les processus métiers s’appuyant sur l’organisation pédagogique définie par le chef d’établissement sont décrits dans la *figure 5* ci-dessous.

![*Figure 5 : Vue d'ensemble des processus s’appuyant sur l’organisation pédagogique*](Figures/Figure2.1.png "*Figure 5 : Vue d'ensemble des processus s’appuyant sur l’organisation pédagogique*")  
*Figure 5 : Vue d'ensemble des processus s’appuyant sur l’organisation pédagogique*

## 2.2. Emploi du temps

L’emploi du temps est le principal référentiel d’organisation pédagogique des établissements scolaire et des écoles.

En tant que **description spatio-temporelle de l’organisation** des **enseignements**, des **activités facultatives** et des **actions d’accompagnement** mis en œuvre dans l’établissement, son élaboration nécessite de prendre en compte conjointement :

* les horaires d’enseignement définis par arrêtés du ministre chargé de l’Éducation nationale ;
* les principes retenus par l’établissement dans le cadre de son autonomie pédagogique et éducative, définie par [l’article R421-2 du code de l’éducation](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000032973703), et portant notamment sur l’organisation de l’établissement en classes et groupes d’élèves, la répartition des élèves, l’emploi des dotations en heures d’enseignement et l’organisation du temps scolaire ;
* les obligations réglementaires de service des enseignants ;
* les contraintes propres à l’établissement (horaires de fonctionnement de l’établissement et de ses services annexes, nature et disponibilité des locaux et des équipements pédagogiques et sportifs, etc.).

Eu égard à ces éléments, l’élaboration de l’emploi du temps dans le second degré incombe donc nécessairement au chef d’établissement dans la mesure où elle procède de l’exercice des compétences qu’il tire des articles [R421-9](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000045581008) et [R421-10](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000024275744) du code de l’éducation en qualité, respectivement, d’organe exécutif de l’établissement et de représentant de l’État.

En mettant en relation, dans l’espace et au cours du temps, les élèves et les personnels chargés de les encadrer, l’emploi du temps porte toutes les informations utiles à la réalisation de contrôles tels que celui de l’assiduité des élèves et, de manière générale, leur surveillance ou la vérification du « service fait » des personnels. De même, c’est nécessairement par référence à l’emploi du temps que sont mis en œuvre les différents actes de régulation que sont, par exemple, les remplacements de courte durée (RCD) des enseignants ou encore les dispositifs d’accompagnement pédagogique au bénéfice des élèves à besoins éducatifs particuliers, tels que visés à [l’article D332-6](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000025375693/2012-04-01) du code de l’éducation.

### 2.2.1. Emploi du temps annuel

Dans le second degré, l’emploi du temps « annuel » est l’emploi du temps dans l’état initial avant toute modification. Il correspond au fonctionnement théorique que serait celui de l’établissement s’il n’était jamais affecté par quelque événement que ce soit : **il décrit une semaine type de l’établissement**.

### 2.2.2. Emploi du temps opérationnel

Dans le second degré, l’emploi du temps « opérationnel » constitue une photographie instantanée et dynamique du fonctionnement pédagogique de l’établissement, tenant compte de tous les événements, prévus et imprévus, affectant l’emploi du temps annuel (absence d’enseignants, aménagements divers, etc.) : **il décrit toutes les semaines de l’année scolaire**, distinctement les unes des autres, et conserve la mémoire des changements intervenus, le cas échéant jusqu’à la granularité du créneau de l’emploi du temps (exemple : dispositifs d’inclusion). 

## 2.3. Vie scolaire

Dans le second degré, la « vie scolaire » est définie, dans le cadre de la définition des missions des conseillers principaux d’éducation (CPE), comme l’activité consistant à « placer les adolescents dans les meilleures conditions de vie individuelle et collective, de réussite scolaire et d'épanouissement personnel ».

Plus largement, dans le langage courant, les logiciels dits de « vie scolaire » outillent notamment le suivi de l’assiduité – telle que définie au code de l’éducation – des élèves (absences, notes, évaluations, bulletins…) assuré par les personnels responsables des activités organisées pendant le temps scolaire.

La notion d’assiduité s’entend au sens des articles du code de l’éducation mentionnés ci-dessous :

* [Article L511-1 ](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006525119): Les obligations des élèves consistent dans l'accomplissement des tâches inhérentes à leurs études ; elles incluent l'assiduité et le respect des règles de fonctionnement et de la vie collective des établissements.
* [Article R511-11 ](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000020743432/): L'obligation d'assiduité mentionnée à l'article L. 511-1 consiste, pour les élèves, à se soumettre aux horaires d'enseignement définis par l'emploi du temps de l'établissement. Elle s'impose pour les enseignements obligatoires et pour les enseignements facultatifs dès lors que les élèves se sont inscrits à ces derniers.
* Les élèves doivent accomplir les travaux écrits et oraux qui leur sont demandés par les enseignants, respecter le contenu des programmes et se soumettre aux modalités de contrôle des connaissances qui leur sont imposées.
* Les élèves ne peuvent se soustraire aux contrôles et examens de santé organisés à leur intention.

*Cf. [décret n° 70-738 du 12 août 1970 modifié](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000874749/).*

## 2.4. Démarche en ligne

Les démarches et formalités administratives concernent l’ensemble des actions et interactions que peut avoir un particulier (famille notamment) ou un professionnel (agent) dans le but d’obtenir un acte administratif par une voie formelle entre lui-même et les institutions représentatives de l’État ou tout autre organisme, qu’il soit public ou privé. Une démarche en ligne est un moyen pour les citoyens de réaliser la formalité à distance par internet.

[Dans son observatoire des démarches essentielles](https://observatoire.numerique.gouv.fr/)[^3], la DINUM recense les démarches administratives nationales les plus utilisées par les usagers et fournit des indicateurs qui permettent de suivre l’avancée de la dématérialisation et la qualité de l'expérience usager.

## 2.5. Pratiques pédagogiques

Une pratique pédagogique désigne la pratique enseignante qui a lieu dans un contexte spatio-temporel donné (principalement dans la classe, mais également en dehors, à travers des dispositifs dédiés impliquant l’action de l’enseignant selon des modalités à distance) et visant l’acquisition de compétences et connaissances transversales et disciplinaires par les élèves. La pratique pédagogique repose sur les programmations pédagogiques annuelles et périodiques (progressions) des enseignants et s’appuie au besoin sur des outils numériques de type « cahier de textes » pour sérier et organiser les activités associées.

## 2.6. Choix, acquisition et utilisation des ressources numériques éducatives

L’acquisition des ressources numériques éducatives, libres ou propriétaires, gratuites ou payantes, est encadrée par le code de l’éducation :

* Article R421-23 du code de l’éducation (au second degré) : « Le Conseil d’administration, sur saisine du chef d’établissement, donne son avis sur : […] les principes de choix des manuels scolaires, des logiciels et des outils pédagogiques […] ».
* Article D411-2 (au premier degré) : « […] une information doit être donnée au conseil d'école sur les principes de choix de manuels scolaires ou de matériels pédagogiques divers […] ».

Les processus liés aux ressources numériques éducatives sont décrits dans la *figure 6* ci-après, du choix à l’accès, avec un processus amont relevant des fournisseurs de ressources afin de permettre le référencement, et un processus aval relevant des porteurs de projet pour suivre les utilisations :

![*Figure 6 : Processus liés aux ressources numériques éducatives*](Figures/Figure2.6.png "*Figure 6 : Processus liés aux ressources numériques éducatives*")  
*Figure 6 : Processus liés aux ressources numériques éducatives*

## 2.7. Parcours pédagogique et parcours de formation

Un parcours pédagogique (apprenant élève) ou de formation (apprenant adulte) est une suite de ressources et d’activités ordonnées de manière scénarisée.

Les parcours scénarisés numériques permettent à l’apprenant d’avancer à son rythme et de poursuivre un objectif d’apprentissage, de développement de compétences ou connaissances, avec des fonctionnalités basées sur la guidance, la différenciation, la personnalisation, la remédiation, la possibilité de non-linéarité, de retour (*feedback*) immédiat, le droit à l’erreur et la présence d’activités sociales.

Un programme de formation est constitué d’un ensemble de parcours (souvent appelés « modules »). Il peut répondre à plusieurs critères : un besoin spécifique de formation, un niveau de compétence à atteindre. Il peut être diplômant ou certifiant.

## 2.8. Orientation, préparation à la vie professionnelle

```
*[À venir]*
```

## 2.9. Pilotage (des missions éducatives)

Le pilotage des missions éducatives implique une démarche de management des activités et dispositifs d’action, sous contraintes organisationnelles, juridiques et financières, dans une perspective stratégique prenant en compte les objectifs essentiels d’enseignement, d’éducation, de socialisation, d’orientation ou encore de préparation à la vie professionnelle.

Il relève de différents niveaux de décision, de l’établissement ou de l’école, jusqu’aux services du ministère chargé de l’éducation nationale, en passant par les académies et collectivités territoriales.

Des dispositifs d’évaluation (outils – positionnement, auto-évaluation, enquêtes - processus – audits, études, méthodes d’intégration dans le suivi des projets) permettent d’outiller les besoins de pilotage de chacun de ces niveaux de décision, afin d’apporter une aide au pilotage de la politique publique d’éducation et des projets de services numériques pour l’éducation en particulier.


[^1]: [Bulletin officiel spécial n° 7 du 11 décembre 2014](https://www.education.gouv.fr/bo/14/Special7/MENE1428315C.htm)

[^3]: [Observatoire de la qualité des démarches en ligne de la DINUM](https://observatoire.numerique.gouv.fr/)