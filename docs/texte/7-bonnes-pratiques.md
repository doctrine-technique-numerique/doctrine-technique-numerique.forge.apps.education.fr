# 7. Bonnes pratiques

## 7.1. Pour des services numériques pour l’éducation écoresponsables

### 7.1.1. Contexte réglementaire

La loi « réduire l'empreinte environnementale du numérique en France » (REEN) du 15 novembre 2021 a pour objectifs clés l’adoption d’usages numériques écoresponsables et prévoit la création d’un référentiel général d’écoconception, fixant des critères de conception durable afin de réduire l’empreinte environnementale de ces services (article 25).

L’achat de services numériques responsables est une des démarches de réduction de l’empreinte du numérique identifié par la loi REEN. Par ailleurs, la loi recommande d’établir une stratégie numérique responsable dans les territoires. Il est prévu que les communes de plus de 50 000 habitants définissent au plus tard le 1<sup>er</sup> janvier 2025 une stratégie numérique responsable (article 35).

### 7.1.2. Achats écoresponsables des services numériques pour l’éducation

Les achats écoresponsables des services numériques font référence à la pratique consistant à sélectionner, acquérir et utiliser des services numériques de manière durable. Cela inclut la prise en compte des impacts environnementaux tout au long du cycle de vie des services numériques, depuis leur conception jusqu'à leur fin de vie.

En complément des exigences pour la fourniture de services décrites au sein du référentiel du numérique responsable dans l’éducation, en annexe de la présente doctrine, des actions précises intégrées dans les achats publics permettront d’orienter l’offre de services vers la prise en compte d’enjeux environnementaux.

Les éléments qui suivent peuvent être intégrés aux appels d'offre pour tout projet de réalisation concernant un service numérique pour l’éducation. En complément, dans la grille d'évaluation des offres, l’écoconception numérique devrait être identifiée précisément comme un critère d'évaluation (poids de 10 % recommandé). 

En réponse aux achats publics de services numériques pour l’éducation, il est important que :

* l'ensemble des mesures annoncées par le candidat couvre toutes les phases du projet (étude initiale, maquettes, recherche usager, développements, tests, maintenance…) ;
* dans le cas de l'intervention de plusieurs prestataires (consortium ou sous-traitants), cette contrainte soit bien prise en compte par l'ensemble des acteurs ;
* le candidat prévoie une formation des personnes qui devront publier des contenus par l'intermédiaire des fonctionnalités développées afin de garantir leur sensibilisation à l'écoconception des communications.

#### Exemples de clauses

Le `<TITULAIRE DU MARCHÉ>` est invité à prendre en compte les critères ci-dessous :

* Critères d'écoconception : le fournisseur s'engage à communiquer les critères d'écoconception respectés dans le cadre du développement du service numérique.
* Engagement envers l'écoconception : le fournisseur est incité à désigner un référent en écoconception au sein de son équipe et s'engage à mettre en œuvre des initiatives concrètes pour minimiser l'impact environnemental du service numérique.
* Transparence environnementale : le fournisseur garantit la transparence en fournissant des informations détaillées sur ses pratiques environnementales, ses politiques d'écoconception, et ses efforts pour réduire l'empreinte carbone.
* Utilisation de technologies durables : le fournisseur est incité à utiliser des technologies durables, y compris des infrastructures basées sur des énergies renouvelables et un hébergement vert.
* Formations : le fournisseur s’engage à former ou sensibiliser à l'écoconception et au numérique responsable les personnes en charge du développement du service numérique (développeurs, chefs de projets, etc.).
* Le fournisseur s'engage à contribuer à accompagner la communauté éducative aux enjeux de l'écoconception.
* Le fournisseur a initié ou s’engage à initier une démarche de mesure d'impact.
* Le fournisseur devra s’auto-évaluer et fournir :
     * un bilan de la démarche d’écoconception qu’il a mise en œuvre en utilisant l’outil [NumEcoDiag](https://ecoresponsable.numerique.gouv.fr/publications/boite-outils/fiches/numecodiag/) ;
     * l’Éco-score des pages « publiques » et des interfaces « utilisateurs » et « administrateur » à l’aide d’EcoIndex.

### 7.1.3. Pratiques écoresponsables en établissements/écoles

Promouvoir des pratiques écoresponsables dans le domaine numérique implique l'adoption de pratiques conscientes et durables. Les usages responsables revêtent une importance significative dans l’éducation et contribuent à :

* réaliser des économies d'énergie et réduire l'empreinte carbone ;
* optimiser les ressources ;
* limiter les déchets électroniques ;
* améliorer la productivité en adoptant des pratiques de travail plus efficientes ;
* sensibiliser aux enjeux environnementaux ;
* respecter les normes environnementales et réglementations ;
* améliorer l’image de l’établissement par l’engagement envers la durabilité et la responsabilité sociale.

Comme cela est précisé dans le guide « [Agir pour la transition écologique dans les écoles, collèges et lycées](https://eduscol.education.fr/1117/education-au-developpement-durable) », le « [Guide de bonnes pratiques numérique responsable pour les organisations](https://ecoresponsable.numerique.gouv.fr/publications/bonnes-pratiques/) » et le document « [L’impact des bonnes pratiques numériques écoresponsables au sein de votre organisation](https://ecoresponsable.numerique.gouv.fr/publications/impact-bonnes-pratiques/) », il est conseillé d’actionner simultanément les différents leviers, à savoir :

* diffuser des bonnes pratiques d’usage du numérique et les intégrer dans le règlement intérieur de l’établissement. Ces bonnes pratiques concernant notamment :
     * la limitation des flux de données (limiter les résolutions des vidéo en streaming, préférer le partage de document aux pièces jointes, etc.),
     * les économies d’énergie (éteindre les appareils non utilisés, éteindre tous les équipements le soir et le week-end, paramétrer la mise en veille automatique des appareils, etc.) ;
* mettre en place une stratégie du numérique responsable à l’échelle de l’école ou de l’établissement. Cela implique notamment :
     * d’inclure un volet numérique responsable dans le projet de la structure scolaire, en impliquant des représentants de la communauté éducative et en lien avec la collectivité territoriale,
     * d’inclure le numérique responsable dans le règlement intérieur de l’établissement,
     * de mettre en œuvre une stratégie d’achat écoresponsable avec la collectivité de rattachement ;
* informer et accompagner le changement :
     * en faisant connaître l’état des lieux et les objectifs du numérique responsable, en lien avec la collectivité,
     * en s’appuyant sur les éco-délégués,
     * en sensibilisant et en proposant des actions de formation auprès de toute la communauté éducative.

## 7.2. Pour des services numériques pour l’éducation accessibles

### 7.2.1. Contexte réglementaire

L'accessibilité numérique concourt à la garantie de l'égalité d'usage et de consultation par les personnes en situation de handicap des services et contenus numériques éducatifs mis à disposition au travers de sites, applications mobiles ou équipements connectés. 

Il s'agit d'une obligation légale pour les organismes publics définie par l'article 47 de la loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées. 

Le niveau légal de conformité est précisé par l'article 5 du décret n° 2019-768 du 24 juillet 2019, relatif à l'accessibilité aux personnes handicapées des services de communication au public en ligne. 

Le référentiel général d'amélioration de l'accessibilité (RGAA), dans sa dernière version en vigueur, est le document de référence en matière de critères d'accessibilité numérique à satisfaire pour les interfaces consultées au travers d'un navigateur web.

Le référentiel d'évaluation de l'accessibilité des applications mobiles (RAAM), dans sa dernière version en vigueur, est le document de référence en matière de critères d'accessibilité numérique à satisfaire pour les interfaces consultées au travers d'une application mobile. 

Si le référencement au RGAA ou au RAAM n'est pas possible ou n'est pas suffisant, la conformité devra alors être évaluée par rapport à la norme européenne EN 301 549, dans sa dernière version en vigueur, complétée par toute autre référence concourant au respect de la législation, notamment parmi les documents proposés par la direction interministérielle du numérique (Dinum) ou équivalents. 

### 7.2.2. Achats de services numériques pour l’éducation accessibles

Les achats accessibles des services numériques font référence à la pratique consistant à sélectionner, acquérir et utiliser des services numériques respectant les critères d’accessibilité numérique. 

En complément des exigences pour la fourniture de services décrites au sein du référentiel du numérique responsable pour l’éducation, en annexe de la présente doctrine, des actions précises intégrées dans les achats publics permettront d’orienter l’offre de services vers la mobilisation de tous les moyens nécessaires afin de viser le niveau de conformité légale.

Les éléments qui suivent peuvent être intégrés aux appels d'offre pour tout projet de réalisation numérique concernant un service numérique pour l’éducation. En complément, dans la grille d'évaluation des offres, l’accessibilité numérique devrait être identifiée précisément comme un critère d'évaluation (poids de 10 % recommandé).

La direction interministérielle du numérique (Dinum) émet également des préconisations en matière de prise en compte de l'accessibilité numérique, disponibles à l’adresse : [https://design.numerique.gouv.fr/outils/accessibilite-marche-public/](https://design.numerique.gouv.fr/outils/accessibilite-marche-public/)

En réponse aux achats publics de services numériques pour l’éducation, il est important que :

* l'ensemble des mesures annoncées par le candidat couvre toutes les phases du projet (étude initiale, maquettes, recherche usager, développements, tests, maintenance…) ;
* dans le cas de l'intervention de plusieurs prestataires (consortium ou sous-traitants), cette contrainte soit bien prise en compte par l'ensemble des acteurs ;
* le candidat prévoie une formation des agents qui devront publier des contenus par l'intermédiaire des fonctionnalités développées afin de garantir leur sensibilisation à l'accessibilité des communications.

### 7.2.3. Exemples de clauses – exigences minimales

* Le `<TITULAIRE DU MARCHÉ>` devra s'assurer que les développements, contenus et fonctionnalités présenteront un niveau de conformité égal à 100 %. 
* En deçà de ce niveau de conformité, une défaillance du prestataire pourra être constatée. Ce niveau d'exigence concerne toutes les fonctionnalités et contenus développés par le `<TITULAIRE DU MARCHÉ>` ainsi que la capacité des éventuelles fonctions d'édition de contenu à produire des contenus conformes. 
* Ce niveau d'exigence ne concerne pas les contenus produits par les équipes de l'`<ORGANISME PUBLIC>` ainsi que les contenus qui feraient l'objet d'une dérogation telle que définie dans les référentiels ou la loi, et validée par l'`<ORGANISME PUBLIC>`.

#### 7.2.3.1. Vérification

* L'`<ORGANISME PUBLIC>` se réserve le droit de faire vérifier, au moyen d'audits ou de recette, par un prestataire expert en accessibilité numérique ou ses propres ressources expertes en accessibilité numérique, le respect des exigences lors de toutes les phases du projet et des livraisons. 
* Dans le cas où le niveau de conformité exigé ne serait pas atteint du fait d'une défaillance du `<TITULAIRE DU MARCHÉ>`, celui-ci devra procéder à toutes les actions correctives nécessaires. Ces actions correctives sont à la charge du `<TITULAIRE DU MARCHÉ>`.

#### 7.2.3.2. Accompagnement expert

* L'`<ORGANISME PUBLIC>` se réserve le droit de recourir à un prestataire externe, expert en accessibilité numérique, afin de l'accompagner dans toutes les phases de réalisation du projet. 
* Dans ce cas, ce prestataire sera le contact technique des équipes du `<TITULAIRE DU MARCHÉ>` pour tout ce qui a trait à l'accessibilité numérique et la conformité attendue de l'ensemble du projet.

#### 7.2.3.3. Éléments de réponse 

* Le `<TITULAIRE DU MARCHÉ>` devra fournir, dans le cadre du présent marché, tous les renseignements nécessaires en indiquant précisément la méthodologie, les moyens et les processus mis en œuvre pour satisfaire à ces exigences.
* En particulier, la méthodologie, les moyens et les processus garantissant la conformité des développements, contenus et fonctionnalités livrés seront détaillés ; ces détails pourront comporter (liste non exhaustive) : 
    * les phases de contrôle (conception, développement, recette...) ; 
    * les moyens de contrôle (audits, tests, recherche utilisateur...) ; 
    * la matérialisation des contrôles (rapport d'audit, résultat de tests...) ; 
    * le ou les outils utilisés aux fins de contrôle (technologies, outils informatiques, aides techniques...).
* Les candidats devront également fournir, dans le cadre du présent marché, tous les renseignements nécessaires à l'évaluation de leur capacité à satisfaire la conformité exigée, en précisant notamment les qualifications et formations à l'accessibilité numérique suivies par ou prévues pour son personnel susceptible d'intervenir dans les réalisations. 

#### 7.2.3.4. Points de vigilance sur les technologies 

* Si le `<TITULAIRE DU MARCHÉ>` identifie une complexité particulière à rendre accessible certaines technologies dont l'usage serait nécessaire pour satisfaire certains besoins exprimés dans le présent document et indispensables au projet, alors il lui appartient de décrire précisément :
    * les points de complexité ou de blocage inhérents à l'utilisation de ces technologies ;
    * les alternatives, lorsqu'elles existent, qu'il serait possible de proposer afin d'assurer à l'utilisateur l'accès aux informations et aux fonctionnalités.
* Les candidats sont libres d'apporter toute autre précision, document ou exemple de livrables qu'ils jugeraient pertinent.

## 7.3. Usages accessibles en établissements/écoles

Favoriser des usages accessibles dans le domaine numérique signifie œuvrer en faveur de pratiques qui garantissent une utilisation inclusive des technologies. Il importe pour cela d’adopter des bonnes pratiques favorisant l'accessibilité des services, tels que :

* Documents accessibles : les documents électroniques (présentations, documents texte, PDF…) doivent être accessibles. Cela peut inclure l'ajout d'alternatives textuelles pour les images, l'utilisation de titres et de balises appropriés, et la vérification de la lisibilité du contenu.
* Contraste des couleurs : choisir des combinaisons de couleurs qui offrent un bon contraste pour améliorer la lisibilité, notamment entre le texte et l'arrière-plan. 
* Sous-titrage des vidéos : les vidéos doivent avoir des sous-titres précis, synchronisés avec le contenu audio, ou des descriptions des sons importants, le cas échéant. De plus, il faut s’assurer que la plateforme utilisée pour partager la vidéo prend en charge le sous-titrage et que les options d'affichage sont facilement accessibles. 
* Adaptations des interfaces utilisateur : utiliser des interfaces utilisateur adaptatives qui permettent aux utilisateurs de personnaliser la taille du texte, la police et d'autres éléments pour répondre à leurs besoins spécifiques.
* Utilisation de polices lisibles : privilégier des polices simples et lisibles, éviter les polices qui peuvent être difficiles à déchiffrer, en particulier pour ceux qui utilisent des technologies d'assistance.
* Accessibilité des réunions et des présentations : s’assurer que les présentateurs décrivent les informations visuelles, utilisent des microphones lorsqu'ils sont disponibles, et partagent électroniquement les documents ou présentations à l'avance.