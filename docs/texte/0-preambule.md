# Préambule

Les investissements massifs des collectivités territoriales et de l’État pour le numérique éducatif ont conduit à de nombreux projets d’équipements et de services pour les établissements scolaires et écoles (matériels collectifs, équipements individuels mobiles, câblage, raccordement à internet, tableaux numériques, ENT, ressources numériques, accompagnement et formation). Dans le second degré, les établissements scolaires complètent cette offre par l’acquisition de logiciels de conception d’emplois du temps et de solutions de vie scolaire, ainsi que par le choix de ressources et services issus des sélections des équipes pédagogiques.

L’utilisation croissante des services numériques pour l’éducation dans les 1<sup>er</sup> et 2<sup>d</sup> degrés, à des fins non seulement administratives et de suivi de la scolarité, mais aussi d’activités pédagogiques et de mise en œuvre des missions éducatives, a pour corollaire une augmentation conséquente de la fréquentation de ces services, qui nécessite la parfaite maîtrise des données et un écosystème ouvert et interopérable.

Deux objectifs sont désormais au cœur de la politique publique du numérique éducatif :

* permettre à l’utilisateur final de bénéficier d’un bouquet de services avec une expérience optimisée, qu’il s’agisse de l’élève, de son responsable, de l’enseignant ou de tout autre acteur de la communauté éducative des écoles et établissements ;
* permettre l’interopérabilité entre tous les services qui concourent au service final rendu à l’utilisateur. 


L’article L.131-2 du code de l’éducation instaure depuis 2013 le service public du numérique : *« Dans le cadre du service public de l'enseignement et afin de contribuer à ses missions, un service public du numérique éducatif et de l'enseignement à distance est organisé pour, notamment :*

1. *mettre à disposition des écoles et des établissements scolaires une offre diversifiée de services numériques permettant de prolonger l'offre des enseignements qui y sont dispensés, d'enrichir les modalités d'enseignement et de faciliter la mise en œuvre d'une aide personnalisée à tous les élèves ;*
2. *proposer aux enseignants une offre diversifiée de ressources pédagogiques, des contenus et des services contribuant à leur formation ainsi que des outils de suivi de leurs élèves et de communication avec les familles ;*
3. *assurer l'instruction des enfants qui ne peuvent être scolarisés dans une école ou dans un établissement scolaire, notamment ceux à besoins éducatifs particuliers. Des supports numériques adaptés peuvent être fournis en fonction des besoins spécifiques de l'élève ;*
4. *contribuer au développement de projets innovants et à des expérimentations pédagogiques favorisant les usages du numérique à l'école et la coopération.*

*[…] Dans le cadre de ce service public, la détermination du choix des ressources utilisées tient compte de l'offre de logiciels libres et de documents au format ouvert, si elle existe. »*

L’État doit donc garantir à chaque acteur de l’éducation une égalité d’accès et un usage simple des services numériques dans un écosystème sécurisé, ouvert et interopérable à des fins de mise en œuvre des apprentissages dans le cadre des programmes et référentiels de compétences du ministère chargé de l’éducation nationale.

**Pour cela, le numérique pour l’éducation doit se développer selon une logique de « plateforme »[^1], au sens d’un ensemble d’acteurs respectant un cadre d’architecture et des règles et standards communs, pour mettre à disposition des usagers un ensemble lisible et structuré de services accessibles simplement et interopérables entre eux. Ce cadre d’architecture et ces règles communes, qui doivent faciliter la circulation des données entre les acteurs publics et privés, sont rassemblés dans la présente « doctrine technique du numérique pour l’éducation »**

[^1]: Au sens de « l’État plateforme », théorisé par Tim O'Reilly dans *Government as a Platform*, et en France par Nicolas Colin et Henri Verdier dans, *L'Âge de la multitude, entreprendre et gouverner après la révolution numérique*.
