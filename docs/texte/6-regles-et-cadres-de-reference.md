# 6. Règles et cadres de référence

## 6.1. Référentiels généraux

Le cadre de référence décrit dans la présente doctrine vient se substituer à un ensemble de documents existants, dont certains vont perdurer le temps de la mise en œuvre effective du nouveau cadre.

La doctrine technique s’appuie sur des exigences transverses portées par des référentiels généraux décrits ci-dessous.

### 6.1.1. RGS (référentiel général de sécurité)

[Le référentiel général de sécurité (RGS)](https://www.ssi.gouv.fr/entreprise/reglementation/ "Le référentiel général de sécurité (RGS)") est un référentiel destiné à sécuriser les échanges électroniques de la sphère publique. Pour une autorité administrative, appliquer le RGS permet de garantir aux citoyens et autres administrations que le niveau de sécurité de ses systèmes d’information est bien adapté aux enjeux et aux risques et qu’il est harmonisé avec ceux de ses partenaires.

### 6.1.2. RGAA (référentiel général d’amélioration de l’accessibilité)

[Le référentiel général d’amélioration de l’accessibilité (RGAA)](https://accessibilite.numerique.gouv.fr/ "Le référentiel général d’amélioration de l’accessibilité (RGAA"), à forte dimension technique, offre une traduction opérationnelle des critères d’accessibilité issus des règles internationales ainsi qu'une méthodologie pour vérifier la conformité à ces critères.

La version 4.1 de ce référentiel a été publiée le 18 février 2021.

### 6.1.3. RGI (référentiel général d'interopérabilité)

[Le référentiel général d’interopérabilité (RGI)](https://www.numerique.gouv.fr/publications/interoperabilite/ "Le référentiel général d’interopérabilité (RGI)") est un cadre de recommandations référençant des normes et standards qui favorisent l'interopérabilité au sein des systèmes d'information de l'administration. Ces recommandations constituent les objectifs à atteindre pour favoriser l'interopérabilité. Elles permettent aux acteurs cherchant à interagir et donc à favoriser l'interopérabilité de leur système d'information, d’aller au-delà de simples arrangements bilatéraux.

### 6.1.4. RGPD (règlement général sur la protection des données)

Le RGPD est le règlement relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données. Avec la directive en matière de protection des données dans le domaine répressif1, il s’inscrit dans le train de mesures européennes sur la protection des données adopté en mai 2016.

Le règlement est entré en vigueur le 24 mai 2016 et s'applique depuis le 25 mai 2018.

Une [description de la protection des données dans l’Union Européenne](https://ec.europa.eu/info/law/law-topic/data-protection/data-protection-eu_fr#autorits-nationales-de-protection-des-donnes) et le [texte du RGPD](https://www.cnil.fr/fr/reglement-europeen-protection-donnees) sont disponibles en ligne. 

### 6.1.5. RGESN (référentiel général d'écoconception de services numériques)

Les objectifs du [référentiel général d’écoconception de services numériques (RGESN)](https://ecoresponsable.numerique.gouv.fr/publications/referentiel-general-ecoconception/) sont de réduire la consommation de ressources informatiques et énergétiques et la contribution à l’obsolescence des équipements, qu’il s’agisse des équipements utilisateurs ou des équipements réseau ou serveur.

La version 1 de ce référentiel a été publiée le 28 novembre 2022.

### 6.1.6. R2GA (référentiel général de gestion des archives)

Le [référentiel général de gestion des archives](https://www.gouvernement.fr/sites/default/files/contenu/piece-jointe/2014/07/r2ga_document_complet_201310.pdf) vise deux objectifs principaux :

* fournir une synthèse claire et abordable de la législation ;
* donner de premières orientations concrètes aux décideurs pour être plus performants en matière de gouvernance de l'information dans leurs services. 

## 6.2. Documents de référence de la doctrine technique du numérique pour l'éducation

### 6.2.1. Cadre général de sécurité des services numériques pour l’éducation

Ce cadre général vise à fournir à chaque acteur d’un dossier, selon ses prérogatives, des exigences permettant d'opérer – en lien avec les autres acteurs compétents et de manière cohérente avec l’ensemble de la doctrine –, les actions requises afin de garantir la sécurité des services pour l’Éducation dans son ensemble.

!!!note "Doctrine"
    Tout acteur qui intervient dans le domaine du numérique éducatif doit respecter le cadre général de sécurité des services numériques pour l’éducation. 

### 6.2.2. Référentiel d’interopérabilité des services numériques pour l’éducation

Ce référentiel vise à décrire les conditions de l’interopérabilité des services numériques pour l’éducation permettant de :

* produire des parcours usagers fluides ; 
* assurer le suivi de l’élève, quelle que soit l’activité qu’il a réalisée et quel que soit l’éditeur qui met à disposition le service numérique correspondant ; 
* échanger des données entre applications de façons éthique et sécurisée ;
* assurer la réversibilité et la réutilisation des données quand c’est nécessaire.

Il précise les exigences sémantiques, syntaxiques et techniques. L’interopérabilité entre services numériques pour l’éducation nécessite la standardisation des formats de données, la définition des modalités d’intégration et des modes de communication.

!!!note "Doctrine"
    Tout acteur qui intervient dans le domaine du numérique éducatif doit respecter le référentiel d’interopérabilité des services numériques pour l’éducation ainsi que les règles d’échanges, basées sur des contrats d’interface pour l’articulation avec les services socles nationaux, sur des nomenclatures partagées, et sur des grands standards « métier » décrits dans ce référentiel. 

### 6.2.3. Référentiel du numérique responsable pour l’éducation

Ce référentiel recense les règles, exigences, bonnes pratiques et recommandations en matière d’écoresponsabilité et d’accessibilité numérique. Il vise à contribuer à la stratégie de l’État pour aller vers la neutralité carbone en 2050.

!!!note "Doctrine"
    Tout acteur qui intervient dans le domaine du numérique éducatif doit respecter le référentiel du numérique responsable pour l’éducation. Dans l’attente de son élaboration, le cadre d’exigences est celui des référentiels généraux pour l’accessibilité et l’écoconception définis au paragraphe 6.5, ainsi que la charte consacrée à la culture et à la citoyenneté numériques.

## 6.3. Documents d’accompagnement des projets territoriaux de services numériques

### 6.3.1. SDET et ENT

Pour définir les services attendus dans les ENT mis à disposition par les porteurs de projet territoriaux, le ministère publie le [SDET (Schéma Directeur des Espaces numériques de Travail)](http://eduscol.education.fr/SDET).

Il sert actuellement de référence dans les marchés publics portés par les collectivités territoriales, et devient opposable par ce biais. En fournissant des principes sur les aspects techniques, juridiques et organisationnels, il permet de fixer des règles communes au plan national et d’assurer un cadre de référence commun aux projets ENT des différents territoires.

Sur le plan technique, le SDET présente une architecture de référence relative à l’organisation des services d’un ENT et à la prise en compte de son écosystème. Il détaille une architecture souple et adaptable permettant à l’ENT de s’adapter aux évolutions et usages numériques. 

Sur le plan fonctionnel, le SDET expose des principes sur les différents services utilisateurs. Sur le plan technique, il s’appuie sur les exigences et règles communes décrites dans la doctrine technique (interopérabilité, sécurité, confidentialité) permettant une maîtrise des composantes fonctionnelles. 

![Figure 9 : Représentation des composantes du SDET (Schéma Directeur des Espaces numériques de Travail)](Figures/Figure6.3.1.png "Figure 9 : Représentation des composantes du SDET (Schéma Directeur des Espaces numériques de Travail)")  
*Figure 9 : Représentation des composantes du SDET (Schéma Directeur des Espaces numériques de Travail)*

Sur le plan juridique, le SDET fixe un périmètre circonscrit et des principes communs aux services et ressources numériques éducatifs accessibles par l’ENT. Il constitue un référentiel dédié aux ENT. Le SDET est doté d’une annexe opérationnelle traitant de certains aspects juridiques spécifiques de l’ENT. Il permet au responsable de traitement de gérer de manière plus aisée la conformité de son projet au regard des contraintes législatives et réglementaires en matière de protection des données.

Le SDET est un ensemble documentaire, composé :

* d’un document principal et une annexe opérationnelle décrivant précisément :
    * les principes fonctionnels, organisationnels, techniques pour guider la formalisation des besoins « métiers » par les porteurs de projet (cahiers des charges), dans la réalisation ou l’adaptation de produits et de services,
    * les principes de mise en œuvre conformes à la doctrine technique, liés à la confidentialité, la réversibilité, l’archivage  et la sécurité des données manipulées par les solutions ENT (alimentation et gestion des annuaires) et échangées avec les services tiers ;
* des documents d’accompagnement visant à simplifier et unifier les pratiques du déploiement des ENT sur le volet conservation et archivage des données et sur le volet juridique.

### 6.3.2. CARMO et projets d’équipement

Le cadre de référence pour l’Accès aux Ressources pédagogiques via un équipement Mobile - CARMO – est un document au service des relations État-Collectivités pour l’élaboration et la mise en œuvre des projets d’équipements mobiles pour l’accès aux ressources pédagogiques numériques. Il est disponible sur [Éduscol](https://eduscol.education.fr/1087/cadre-de-reference-carmo-version-30 "Cadre de référence CARMO version 3.0").

Il est complété par des recommandations à destination des projets s’inscrivant dans une démarche BYOD/AVEC, qui ont des problématiques spécifiques, décrites dans le « [Guide des projets pédagogiques s'appuyant sur le BYOD/AVEC](https://eduscol.education.fr/document/48422/download "Guide des projets pédagogiques s'appuyant sur le BYOD/AVEC") ».

### 6.3.3. Référentiel CARINE

Le référentiel CARINE (Cadre de référence des services d'infrastructures numériques d'établissements scolaires et d'écoles) a pour objectif de fournir un cadre de référence commun aux acteurs décisionnaires des écoles, établissements scolaires, aux inspecteurs d'académie, aux recteurs, aux responsables des collectivités territoriales, ainsi qu'aux éditeurs de solutions et prestataires de services, pour concevoir, choisir, mettre en place et maintenir les infrastructures numériques d’EPLE et d’école. Il annule et remplace le précédent cadre de référence des S2i2e (Services intranet et internet d'établissements scolaires et d'écoles).

Il est disponible sur [Éduscol](https://eduscol.education.fr/1078/referentiel-s2i2e-carine "Référentiel S2i2e - CARINE").

