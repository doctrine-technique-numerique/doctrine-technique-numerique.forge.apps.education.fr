# 5. Données et système d’information (SI) d’éducation

Les données d’éducation sont protéiformes, issues de divers systèmes d’information et englobent à la fois les données des acteurs de l’éducation que les données des ressources ou services de l’éducation. Aucun d’entre eux ne dispose seul de l’ensemble des données. Aussi, le partage et l’utilisation de la « donnée d’éducation » est un enjeu pour tous. En effet, si des principes communs pour la gestion des données doivent permettre d’assurer l’interopérabilité des systèmes, leur exploitation et leur analyse ouvrent des perspectives d’usages au bénéfice des élèves, des familles, des enseignants et des acteurs de l’éducation : développer des outils pour améliorer le quotidien des enseignants, proposer aux élèves des parcours différenciés et personnalisés, analyser les erreurs et les réussites, expérimenter de nouvelles pratiques pédagogiques, développer de nouvelles connaissances scientifiques fondées sur les neurosciences, créer des opportunités de recherche, améliorer les SI de gestion, etc.

Le pilotage de la donnée et par la donnée doit devenir une réalité dans un écosystème interopérable et sécurisé. Cela impose de définir les données du périmètre, ainsi que les principes communs, afin d’orienter les actions, impliquant les acteurs de l'éducation, dont les collectivités territoriales, la recherche, les entreprises de services numériques (ESN), de l‘EdTech et les associations.

Ce chapitre vise à décrire les caractéristiques de la donnée d’éducation ainsi que les principes de ses usages en gestion ou utilisation, afin de répondre aux attentes de l’ensemble des acteurs et aux enjeux autour des échanges.

## 5.1. Définition dess données du périmètre du numérique éducatif

### 5.1.1. Données d’éducation

Les services numériques éducatifs sont des traitements informatiques, qui reposent sur l’utilisation de données pour fonctionner et qui produisent eux-mêmes des données issues de leur utilisation.

L’ensemble de ces données entre dans le champ des « données d’éducation », et constitue le périmètre de la présente doctrine technique.

### 5.1.2. Caractérisation des données d'éducation

#### 5.1.2.1. Données personnelles / non personnelles

Les données personnelles sont définies à l’article 4, du RGPD comme « toute information se rapportant à une personne physique identifiée ou identifiable ».

Elles font l’objet d’une protection spécifique et ne sont pas concernées par l’ouverture par défaut et la libre réutilisation des données et codes sources publics. Ces principes sont décrits au paragraphe 5.4.

Les données à caractère non personnel se définissent comme toutes les données autres que les données à caractère personnel au sens rappelé *supra*. Elles concernent donc :

* les données qui, dès le départ, ne concernent pas une personne physique identifiée ou identifiable ;
* les données qui, bien qu’initialement à caractère personnel, ont ensuite été rendues anonymes.

#### 5.1.2.2. Données de gestion / données publiques

Les données de gestion permettent au quotidien le fonctionnement et le suivi statistique des services numériques participant au système éducatif. Elles sont caractérisées par leur nécessaire précision et par leur disponibilité (immédiateté de la valeur dans le temps). Elles relèvent souvent de données à caractère personnel.

Les données de la sphère publique désignent toutes les informations rassemblées, créées, conservées ou éditées par les administrations, les services publics et les entités de droit privé ou de droit public poursuivant une mission de service public. Elles peuvent être mises à disposition du plus grand nombre, par l’intermédiaire de jeux de « données ouvertes » ou « open data » (notamment sur le site interministériel [data.gouv.fr](https://www.data.gouv.fr/fr/) dans les conditions définies par la « Licence Ouverte / Open Licence »). Elles sont librement réutilisables.

#### 5.1.2.3. Données / méta-données / jeux de données

Un jeu de données est une collection de données liées à un sujet ou un thème en particulier. Il fait généralement intervenir des données structurées dans un but spécifique et liées à un même sujet. Ces données, appelées données primaires, sont régulièrement accompagnées de « métadonnées » qui désignent des informations enregistrées, structurelles ou descriptives, relatives à ces données et permettant de les préciser. Comme les données primaires, les métadonnées peuvent inclure des données à caractère personnel. 

## 5.2. Vison unifiée des données d'éducation

Pour assurer la continuité numérique, tout au long du parcours usager et des services numériques qu'il convoque, et permettre des échanges standardisés pour communiquer avec les partenaires et fournisseurs de services et ressources, il convient de convenir d'un cadre permettant l'interopérabilité. Cela nécessite de définir des formats de données communs et de spécifier un ensemble de normes et protocoles.  Il s'agit notamment de pouvoir fournir directement à l'ensemble des processus qui en ont besoin, de la conception à l'utilisation, un schéma commun de méta-données et nomenclatures. L'ensemble des aspects liés à l'interopérabilté est développé dans un référentiel dédié. 

### 5.2.1. Schéma des données d'éducation

Les données d’éducation peuvent être décrites dans des dictionnaires de données, modèles de données et transmises selon des structures d’échange qui permettent un dialogue entre différents acteurs, dans une perspective de fiabilité et de disponibilité de la donnée aux acteurs habilités.

Pour normaliser les échanges de données en volume et en qualité de façon à permettre aux différents acteurs de mieux dialoguer, on peut se mettre d’accord sur une structuration commune. On peut notamment distinguer, dans les données d'éducation, les données des acteurs et les données des ressources ou services.  Ces deux grandes familles de données peuvent contenir des données primaires ou des méta-données.  Le schéma par fonction représenté sur la Figure 7 est une illustration d'une telle organisation.  Ce schéma est utilisé dans le référentiel d'interopérabilité en annexe de la présente doctrine, afin de décrire les exigences et standards d'interopérabilité pour le périmètre du numérique éducatif.

![Figure 7 : Schéma des données d'éducation](Figures/Figure5.2.png "Figure 7 : Schéma des données d'éducation")
*Figure 7 : Schéma des données d'éducation[^1]*

[^1]Cette proposition et représentation s’inspire des travaux de [Brandt Redd](https://brandtredd.org/) qui sont exposés sur [EdMatrix](https://www.edmatrix.org/) sous licence Creative Commons 4.0.

Le référentiel d’interopérabilité documente et agrège les standards permettant le fonctionnement de cet ensemble structuré d'informations, afin de constituer un cadre commun aux services numériques généralement utilisés par plusieurs entités, plusieurs SI et applications.

### 5.2.2. Nomenclature des données d'éducation

En complément de la vue référentielle présentée ci-dessus, des listes de valeurs de référence sont nécessaires pour les objets métiers. Différentes nomenclatures sont ainsi définies pour y répondre (les objets sont décrits par leurs caractéristiques, dont certaines peuvent s'appuyer sur des nomenclatures). 

Les nomenclatures de la BCN (Base Centrale des Nomenclatures) constituent le langage commun du système d'information du ministère chargé de l’éducation nationale. Elles sont mises à jour régulièrement et historisées au moyen de dates d'ouverture et de fermeture. Elles sont en consultation sur le site de la [Base centrale des nomenclatures](https://infocentre.pleiade.education.fr/bcn/).

!!!note "Doctrine"
    Tout acteur qui intervient dans le domaine du numérique éducatif doit respecter le schéma de données ainsi que les nomenclatures métier (BCN) ou listes de valeurs répertoriées au référentiel d’interopérabilité.

## 5.3. Pilotage de la donnée et par la donnée

Inscrit dans la stratégie du numérique pour l’éducation, le pilotage par la donnée vise à mobiliser les données au service de l’école, en cohérence avec les politiques gouvernementale et européenne pour l’ouverture, le partage et la valorisation des données, des codes sources et avec le développement des API à destination des autres acteurs publics. Les politiques d’« open data » ne suffisent pas toujours pour répondre aux attentes des partenaires, en particulier les établissements et collectivités territoriales, qui ont un besoin de données de gestion et d’activité scolaire en temps réel (données régulièrement actualisées) pour répondre aux exigences de l’exercice de leurs compétences propres.

Il convient donc, et la présente doctrine technique vise à y contribuer, de mettre en place les modalités d’échange de données entre les partenaires, sur la base de moyens contractuels ou conventionnels, de règles communes, de principes d’architecture partagés et de standards communs, ou encore de plateforme d’échanges.

### 5.3.1. Gouvernance des données

En fournissant des normes, principes et règles régissant les échanges de données selon leur type, la présente doctrine et ses référentiels contribuent à la gouvernance des données au niveau national. Compte tenu du volume important des données d’éducation, il est indispensable de mettre en place un système clair et défini de gouvernance de la donnée. Il permettra non seulement d’assurer la qualité des traitements, mais aussi d’assurer la sécurité, la confidentialité, l'exactitude, la disponibilité et l'exploitabilité des données.

La gouvernance des données doit se mettre en place à toutes les échelles territoriales, pour couvrir l’ensemble des flux de données du périmètre éducatif. Elle s’appuie sur des acteurs, des moyens organisationnels et juridiques.

### 5.3.2. Acteurs et instances associées

La gouvernance des données d’éducation nécessite d’impliquer la diversité des acteurs, issus des secteurs public, privé voire associatif.

Elle se met en place progressivement à tous les niveaux, en mobilisant des acteurs qui ont des rôles différents :

* acteurs de la protection des données : DPD, RSSI, CNIL, Drane, Drasi ;
* acteurs de l’ouverture des données et de l’exploitation des données pour la modélisation cognitive pour l’éducation : Amdac, recherche, EdTech, services statistiques centraux et académiques, collectivités territoriales ;
* acteurs de l’éducation aux données : Amdac, Drane, Réseau Canopé, Clémi.

Au sein du ministère de l’éducation, l’administrateur ministériel des données, des algorithmes et des codes sources (Amdac) est chargé d’animer et de coordonner la politique ministérielle d’ouverture des données, qu’il s’agisse des données produites ou collectées par le ministère ou des données, de nature privée, produites lors de la mise en œuvre du service public de l’éducation, mais également de la mise en place de la gouvernance associée.

Il coordonne l’instruction de chaque proposition ou demande d’ouverture et de diffusion de données et met en place un comité stratégique de la donnée, présidé par le Secrétaire général et comprenant l’ensemble des directeurs d’administration centrale et des représentants des régions académiques.

Depuis 2022, un réseau de référents « données algorithmes et codes sources » dans chaque direction de l’administration centrale, opérateur de l’Éducation nationale et chaque région académique est réuni régulièrement.

Enfin, le ministère a instauré en 2019 un comité d’éthique pour les données d’éducation chargé de conduire des réflexions sur l’utilisation de ces données, afin notamment de garantir un juste équilibre entre valorisation et protection des données, et d’émettre des avis et des recommandations.  Il est composé de parlementaires, de chercheurs et universitaires de différentes disciplines (droit, informatique, sciences de l’éducation, sociologie...), de membres de l'éducation nationale, de représentants des EdTech et de responsables associatifs.

Le ministère dispose d’un grand nombre de dispositifs permettant le dialogue avec les communautés de réutilisateurs et les écosystèmes, au premier rang desquels figurent les différentes associations représentatives comme les associations de collectivités territoriales, réunies par la DNE au sein d’un comité des partenaires, ou les associations représentant les entreprises de l’EdTech au sein du comité de filière.

### 5.3.3. Dispositifs organisationnels et techniques

Des dispositifs techniques et organisationnels, existants ou cibles, visent à rendre opérationnelles les exigences légales (notamment de protection des données personnelles), tout en outillant une stratégie pour la gestion des données et leur mise à disposition des différents acteurs de l'éducation (élèves, familles, enseignants, éditeurs de ressources pédagogiques...). Les principaux dispositifs opérationnels ou en phase avancée de développement sont : 

* fédération d’identité, pour permettre le partage de données d’identité entre des acteurs habilités (via ÉduConnect pour les élèves et responsables, le guichet Agents pour les agents de l’éducation nationale) ;
* le gestionnaire d’accès aux ressources (GAR), pour permettre le partage de données d’identité et d’organisation pédagogique avec les fournisseurs de ressources choisis par les responsables de traitement en établissement, académie ou collectivité ;
* OMOGEN-API pour outiller la politique API du ministère vis-à-vis de ses partenaires ;
* des dispositifs conventionnels et une plateforme d'échanges de données ministère-collectivités territoriales : convention entre le ministère et Régions de France (en cours d’actualisation), conventions d'échanges de données des autorités académiques avec les régions (idem) et avec d'autres collectivités territoriales (à systématiser). Une plateforme dédiée aux échanges de données entre les collectivités territoriales et le ministère vise à outiller la coordination et l’utilisation partagée des données dans le domaine de l'éducation et du pilotage de la politique publique ;
* –	un guichet pour des demandes d'interfaçage au système d'information de gestion et aux services socles est en cours d'organisation dans l’objectif d’accueillir, qualifier, orienter les demandes des acteurs industriels ou partenaires qui souhaitent s’interfacer au SI et services socles ou disposer d’informations sur ces interfaçages.

Chacun de ces dispositifs repose sur des contrats identifiant les responsabilités, données transmises et modalités de l’échange. La gouvernance permet d’instaurer des modalités d’échanges pour une régulation transversale, par domaine. 

### 5.3.4. Cycle de vie des données

Le cycle de vie des données décrit les processus liés aux données par lesquels de la valeur est créée à partir des données : la création, la collecte, le stockage, l'utilisation, la protection, l'accès, le partage, la suppression, le transport et l’archivage.

Tout service numérique produisant ou traitant des données d'éducation doit expliciter l'ensemble des processus participant au cycle de vie de ces données et les soumettre pour validation. 

### 5.3.5. Lignage des données

Pour avoir une connaissance approfondie des données, il est indispensable de mettre en place une maintenance constante de la description technique des données, une diffusion systématique de la connaissance des données, ainsi qu'une gestion rigoureuse du lignage des données, permettant de retracer leur parcours depuis leur création jusqu'à leur suppression.

La traçabilité des données consiste à documenter leur parcours depuis leur origine jusqu'à leur utilisation actuelle. Cela renforce la transparence et la fiabilité des informations. En identifiant les étapes de transformation et les éventuelles anomalies, le lignage contribue à améliorer la fiabilité des données, renforçant ainsi la confiance dans les analyses et les prises de décision

### 5.3.6 Conservation et archivage des données

La mise en œuvre d’un service numérique éducatif nécessite une organisation spécifique des données produites ou stockées. Lorsque ces données sont produites dans le cadre de l’exécution d’une mission de service public, elles constituent des archives publiques en application de l’article L211-1 du code du patrimoine (ce qui concerne notamment les données produites dans les espaces d’échange des ENT). Des contraintes réglementaires spécifiques s’appliquent alors à leur gestion, depuis leur création et jusqu’à leur conservation ou leur élimination.

Pour mettre en œuvre une politique de conservation des données, trois états de la donnée doivent être considérés :

* courant (de la production/réception à la fin de l’usage fréquent, en passant par les phases de validation),
* intermédiaire (usage occasionnel mais régulier),
* définitif (échéance légale ou interne et application du sort final).

Le « sort final » correspond à trois possibilités de traitement à appliquer à la donnée : conservation, élimination ou tri et conservation d’un échantillon. Il s’applique donc à l’expiration du délai d’utilité administrative ou pratique.

La « durée d’utilité administrative » (DUA) est fixée réglementairement et correspond à la fin de l’utilisation fréquente de l’information consolidée au terme de la durée de conservation légale.

La « durée d’utilité pratique » (DUP) s’applique sur la même période, mais en l’absence de texte, elle est définie en commun accord entre le service producteur et l’instance en charge du contrôle scientifique et technique (CST).

Les dispositions législatives, réglementaires mais aussi normatives auxquelles sont soumises les données publiques sont destinées à garantir leur intégrité, leur authenticité, leur qualité et leur sécurité et ce dans un but de réutilisation pour justification de l’activité ou valorisation. La stratégie de gestion des données est décrite dans la politique d’archivage du ministère.

Le statut d’archives publiques s’acquière dès la création de la donnée. La modélisation des processus de production des données prend en compte leur gestion à la source, leur conservation, leur transfert et leur élimination. Les outils intègrent des fonctionnalités d’authentification, d’évaluation, de traçabilité et ce au long des différentes phases de vie et des données : courantes, intermédiaires et définitives. 

La gestion du cycle de vie de la donnée nécessite la mise en œuvre de métadonnées permettant l’évaluation et donc la sélection et l’apposition d’un sort final (échantillonnage, conservation ou élimination) en application de la réglementation en vigueur. 

Pour rappel, l’élimination des données publiques est conditionnée à la validation de l’instance assurant le contrôle scientifique et technique de l’État. Hors ce cadre, toute élimination en masse des données au sein des applications métiers est passible de sanctions pénales (articles L 214-3 et L 214-4 du code du patrimoine).

Concernant la conservation pérenne des données sélectionnées, seuls les systèmes d’archivage électronique certifiés NF Z 42-013 ou ISO 14641-1 sont habilités à accueillir des archives publiques. L’ajout de métadonnées, le management des données, l’utilisation de formats spécifiques différencient ces systèmes de tout autre moyen de stockage.

La conservation des données publiques répond donc à des exigences réglementaires. Chaque acteur est sensibilisé aux responsabilités liées à son périmètre d’intervention. Producteur, utilisateur, gestionnaire, archiviste et administrateur interagissent dans ce cycle d’optimisation de la gestion des données.

Le document « Kit de conservation et d’archivage des données ENT » en annexe du [SDET](https://eduscol.education.fr/ent) fournit les DUA fixées réglementairement correspondant aux services numériques du périmètre des ENT. 

## 5.4. Principes de protection des données d'éducation

### 5.4.1. Protection des données personnelles

Les données à caractère personnel font l’objet d’une protection spécifique. Elles ne sont pas concernées par l’ouverture par défaut et la libre réutilisation des données et codes sources publics. Dans le cas des données personnelles qui relèvent de la vie privée, ces données ne sont pas communicables à des tiers, et *a fortiori* non publiables. Dans le cas des données personnelles non couvertes par le secret de la vie privée, la publication en ligne est proscrite, sauf s’il est possible d’anonymiser le document administratif qui contient des données personnelles. L’anonymisation ne doit pas permettre d’identifier un individu dans la base, ou par le croisement de plusieurs bases, de relier entre eux des ensembles de données distincts concernant un même individu, ni de déduire de l’information sur un individu.

La prise en compte du RGPD renforce l’importance de l’enjeu d’une protection des données personnelles autour de 5 principes majeurs relevant des droits fondamentaux des citoyens européens et de leur droit au respect de la vie privée :

* le principe de finalité : les données à caractère personnel ne peuvent être recueillies et traitées que pour un usage déterminé et légitime ;
* le principe de proportionnalité et de pertinence des données : seules doivent être traitées les informations pertinentes et nécessaires au regard des objectifs poursuivis ;
* le principe d’une durée de conservation des données limitée : les informations ne peuvent être conservées de façon indéfinie dans les fichiers informatiques. Une durée de conservation précise doit être déterminée en fonction de la finalité de chaque fichier ;
* le principe de sécurité et de confidentialité des données : le responsable de traitement est astreint à une obligation de sécurité : il doit prendre les mesures nécessaires pour garantir la confidentialité des données et éviter leur divulgation à des tiers non autorisés et éviter la perte d’intégrité des données ;
* le principe du respect des droits des personnes : information des personnes, droits d’accès et de rectification, droit d’opposition, droit à l'oubli.

De plus, la réutilisation des informations contenues dans les données personnelles est encadrée. Cette réutilisation doit en particulier être licite, c’est-à-dire que toute réutilisation doit être fondée sur l’article 6-1 du RGPD qui exige le consentement des personnes pour un ou plusieurs traitements aux finalités définies et l’existence de fins légitimes poursuivies par le responsable du traitement. De plus, le responsable du traitement doit assurer la sécurisation des données.

### 5.4.2. Sécurisation des données

Sous-ensemble de la sécurité des systèmes d’information, décrite dans le cadre général de sécurité en annexe de la présente doctrine, pour le périmètre du numérique éducatif, la sécurisation des données est une obligation pour le responsable de traitement, afin de garantir la sécurité des données qu’il a collectées et éviter leur divulgation à des tiers non autorisés. 

Le RGPD précise que la protection des données personnelles nécessite de prendre les « mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque ». Cette exigence s’impose aussi bien au responsable du traitement de données personnelles qu’aux sous-traitants impliqués (article 32 du RGPD).

La CNIL fournit un [guide pour la mise en place de précautions élémentaires](https://www.cnil.fr/fr/principes-cles/guide-de-la-securite-des-donnees-personnelles) qui devraient être mises en œuvre de façon systématique. Ce guide est notamment destiné aux DPO (délégués à la protection des données), RSSI (responsables de la sécurité des systèmes d’information) et informaticiens. 

## 5.5. Principe d'ouverture des données d'éducation

La politique de partage des données d’éducation repose sur une approche équilibrée entre la transparence et la protection de la vie privée. La présente doctrine technique contribue à la formalisation de règles de partage et d'accès aux données d’éducation, en s’appuyant notamment sur les « services socles » définis au chapitre 4 et sur les exigences d’interopérabilité décrites dans le référentiel en annexe.

Le cadre juridique relatif à l'*open data* permet de concilier protection de la vie privée et mise à disposition comme la réutilisation des données.

### 5.5.1. Partage et utilisation des jeux de données d'éducation

Le ministère s’inscrit dans les orientations fixées par la circulaire du 27 avril 2021 sur la politique publique de la donnée avec l’ouverture d’un nombre croissant de jeux de données et la diffusion des codes sources des grandes applications nationales. 

Le partage de jeux de données est basé sur les besoins exprimés par les utilisateurs, les mises à disposition de nouveaux jeux de données concernant prioritairement l’information des parents, des élèves ( (carte des formations, internats, diplômes professionnels, etc.), des enseignants, des cadres de l’Éducation Nationale, des chercheurs, des associations, des collectivités territoriales, des opérateurs économiques et de tout citoyen.

La plateforme [data.education.gouv.fr](https://data.education.gouv.fr/pages/accueil/) permet aujourd’hui la consultation de jeux de données intéressant l’usager et le citoyen : calendrier scolaire, annuaire des établissements, résultats d’examens, etc.

S’agissant de la publication des algorithmes et des codes sources libres, le ministère a réalisé en 2023 une plateforme [https://gitlab.mim-libre.fr/men](https://gitlab.mim-libre.fr/men) et une campagne est menée en 2024 pour publier en priorité les algorithmes et les codes sources liés à des décisions individuelles.

### 5.5.2. API-sation des services numériques éducatifs

La mise en œuvre des échanges de données par API permet d'assurer la fluidité des échanges entre services numériques afin d’améliorer et enrichir les parcours usagers d’une part (cf. référentiel d’interopérabilité en annexe) et de mettre à disposition les données en lien avec la stratégie d’ouverture.


<!-- Afin d’encourager cette démarche, qui permet de partager des données d’éducation de manière sécurisée, le ministère met à disposition plusieurs outils pour la gestion des API :

* Cadre normatif et gouvernance des API : [https://portail.forge.education.gouv.fr/#/referentiels/referentiel_men/information/gouvernance_apireferentiels/referentiel_men/information/gouvernance_api](https://portail.forge.education.gouv.fr/) 
* Catalogue des API : [https://pr-apim.omogen.in.phm.education.gouv.fr/#!/](https://pr-apim.omogen.in.phm.education.gouv.fr/) 
* Modèle contrat d’interface : portail Forge (education.gouv.fr)  -->

!!!note "Doctrine"
    Tout acteur qui intervient dans le domaine du numérique éducatif doit respecter les règles de partage, d’échange et de normalisation pour pouvoir ouvrir les données entre les services numériques dans un cadre maîtrisé.
