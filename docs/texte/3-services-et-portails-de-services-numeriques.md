# 3. Services et portails de services numériques

## 3.1. Projets numériques territoriaux et projets d’établissements/écoles

### 3.1.1. Projets territoriaux de services d’infrastructure et de sécurité

Les services d’infrastructures numériques des écoles et établissements ont pour objectif principal la fourniture d’un ensemble de services indispensables à l’accès des services de plus haut niveau dans des conditions de sécurité adaptées aux risques numériques portant sur les SI et les usagers.

Ils permettent principalement la fourniture et la gestion du parc de terminaux (fixes ou mobiles), l'accès authentifié aux postes de travail, puis au réseau de l'établissement ou de l'école et enfin à internet, la mise en œuvre de dispositifs de filtrage destinés à assurer la protection des mineurs dans leurs usages des services numériques (dans le cadre scolaire), ainsi que la supervision, l'exploitation et le maintien en condition opérationnelle et de sécurité de l'ensemble, dans le respect des règles de sécurité applicables et des obligations réglementaires.

!!!note "Doctrine"
	Les services d’infrastructures numériques choisis pour les écoles et établissements respectent le cadre de référence des services d’infrastructures numériques d’établissements scolaires et d’écoles ([CARINE](https://eduscol.education.fr/1078/referentiel-s2i2e-carine)).

Les collectivités territoriales ont la charge de l’acquisition et de la maintenance des infrastructures et des équipements, dont les matériels informatiques et les logiciels prévus pour leur mise en service, nécessaires à l’enseignement et aux échanges entre les membres de la communauté éducative. Une réflexion territoriale permet d’associer les services académiques (ou la DRAAF pour l’enseignement agricole) et les collectivités, afin d’identifier les besoins, moyens et préconisations liés aux infrastructures numériques.

### 3.1.2. Projets territoriaux de services numériques pour l'éducation

Les projets territoriaux de services numériques pour l’éducation fournissent aux écoles et établissements scolaires un bouquet de services numériques diversifiés permettant de prolonger et d’appuyer le service public d’éducation, d’enrichir les pratiques pédagogiques et de faciliter la mise en œuvre des actions éducatives.

!!!note "Doctrine"
	Les projets territoriaux de services numériques pour l’éducation reposent sur un partenariat formalisé entre collectivités territoriales et académies, et s’inscrivent dans le cadre national constitué de la doctrine et ses référentiels, permettant d’assurer l’interopérabilité, la sécurité, la protection des données ainsi que l’harmonisation de l’ensemble des services numériques pour l’éducation à l’échelle nationale. Les acteurs industriels choisis par les porteurs de projet territoriaux s’engagent par le biais de contrats, en respect des règles de la commande publique.

Les projets d’ENT sont historiquement construits selon cette doctrine, encadrés par le SDET (schéma directeur des espaces numériques de travail), les conventions de partenariat État-collectivités et les contrats de commande publique liant les exploitants d’ENT à leurs donneurs d’ordre. 

Le cadre de référence pour l’ensemble des services numériques pour l’éducation est constitué des exigences de la présente doctrine technique du numérique pour l’éducation, des référentiels associés et d’outils d’accompagnement spécifiques, dont le SDET.

### 3.1.3. Projets territoriaux d’équipements mobiles

Les projets territoriaux d’équipements mobiles permettent de doter les usagers en équipement individuel, les établissements/écoles en équipements mobiles partagés et/ou de gérer les services disponibles sur les équipements mobiles utilisés par les acteurs de la communauté éducative de l’établissement ou école.

!!!note "Doctrine"
	Les projets territoriaux d’équipements mobiles respectent le cadre de référence pour l'accès aux ressources pédagogiques via un équipement mobile (CARMO) décrits au *paragraphe 6.2.2.CARMO et projets d’équipement*.

Les projets s’inscrivant dans une démarche BYOD/AVEC ont des problématiques spécifiques, décrites dans le « [Guide des projets pédagogiques s'appuyant sur le BYOD/AVEC](https://eduscol.education.fr/document/48422/download) ».

### 3.1.4. Projets d’établissement de services numériques pour l'éducation

Les établissements scolaires choisissent des outils dans le cadre de leur autonomie pédagogique et éducative, définie par [l’article R421-2 du code de l’éducation](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000032973703) pour les établissements du 2<sup>d</sup> degré, et mettent en œuvre les traitements correspondants. Dans le 1<sup>er</sup> degré, [l’article D411-2 du code de l’éducation](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000028160058) décrit le projet d’école et le rôle du conseil d’écoles (le responsable de traitement est l’IA-DASEN).

!!!note "Doctrine"
	Les outils choisis par les établissements scolaires et les écoles portent sur la mise en œuvre effective de l’ENT proposé par la collectivité territoriale et l’académie, les solutions d’emploi du temps, les briques de vie scolaire quand elles ne sont pas utilisées ou pas présentes dans l’ENT, ou encore les ressources numériques éducatives.  
	Ils s’inscrivent dans le projet d’établissement et participent à la politique documentaire de l’établissement scolaire. Ces choix permettent également de mettre en œuvre des obligations, relativement au cahier de textes numérique ou encore au livret scolaire.

En tant que responsable de traitement (ou responsable conjoint avec la collectivité territoriale de rattachement, notamment dans le cadre des ENT), il appartient au chef d’établissement ou à l’IA-DASEN de respecter les obligations établies par le RGPD et la loi n° 78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés. L’existence de projets territoriaux respectueux des cadres de référence nationaux permet d’accompagner les responsables de traitement dans la mise en conformité de leurs traitements.

En plus des formalités RGPD, le chef d’établissement ou l’IA-DASEN doit s’assurer que chacun des outils numériques qu’il choisit respecte les textes réglementaires et les **nomenclatures nationales** en vigueur, y compris pour les logiciels de suivi des évaluations. Cela permet d’éviter tout risque de discordance des données nécessitant ensuite de corriger les informations erronées lors des phases de dialogue entre ces applications.

## 3.2. Démarches en ligne : portail Scolarité services

« Scolarité services » est un portail pour les parents et représentants légaux d’élèves des écoles, collèges et lycées proposant un accès unique aux services et démarches liés au suivi de la scolarité de leurs enfants. Ce portail propose aussi des services de scolarité aux établissements, décrits au *paragraphe 3.3*.

!!!note "Doctrine"
	Les démarches liées au suivi de la scolarité des élèves sont outillées par des services en ligne mis à disposition par le ministère chargé de l’éducation nationale, afin de faciliter et centraliser l’accès à ces démarches ponctuelles.  
	Cela répond à l’obligation pour l’État d’offrir aux familles la possibilité de réaliser les démarches en ligne. Leur utilisation est développée et encouragée, mais n’est pas exigée des usagers. 

Certaines démarches sont saisonnières, au sens où elles ne sont disponibles qu’à une période définie de l’année (demande de bourses au collège et au lycée, orientation après la 3<sup>e</sup> et après la 2<sup>de</sup> générale et technologique, affectation au lycée, inscription au collège ou au lycée). D’autres permettent l’actualisation des données administratives courantes (fiche de renseignements – collège, lycée), ou le suivi scolaire (livret scolaire unique numérique – LSU, du CP à la 3<sup>e</sup> – puis livret scolaire des lycées – LSL).

## 3.3. Offre de services numériques de l’État basés sur des communs numériques

L’État met à disposition des porteurs de projets territoriaux et établissements scolaires une offre de services permettant de contribuer à l’égalité d’accès sur le territoire, à certains services jugés essentiels et stratégiques. Il met également à disposition les services numériques nécessaires à la formation de ses agents.

!!!note "Doctrine"
	L’État met à disposition deux services de visio-conférence. Un service nommé *Classe virtuelle* pour une utilisation avec les élèves dans les écoles et les établissements scolaires ; un service nommé *Visio-agents* pour tout autre usage.

Disponible sur la plateforme [apps.education.fr](https://apps.education.fr/), ce service repose sur une solution libre (BigBlueButton) et sur un hébergement industriel français conforme à la doctrine de l’État en matière d’informatique en nuage (*cloud*). En conséquence, son utilisation devrait primer sur toute autre solution non souveraine et non libre.

Comme les autres outils de [apps.education.fr](https://apps.education.fr/), le service Classe virtuelle est mis à disposition *via* un lien dans les services et portails de services numériques territoriaux sous condition d’authentification et de création de salle sur le service par l’agent. Dans le cas de l’authentification du portail de services numériques par ÉduConnect, l’accès ne nécessite pas de réauthentification des élèves et des représentants légaux.

!!!note "Doctrine"
	L’État met à disposition des outils, listés ci-dessous, permettant de créer, mutualiser et partager des ressources pédagogiques, des plateformes de conception de parcours pédagogiques d’apprentissage et de formation en ligne à destination des enseignants et de leurs élèves, des agents (formation initiale, continuée et continue).

L’écosystème de plateforme de conception de parcours pédagogiques basé sur Moodle est composé de :

* **Magistère** pour permettre aux formateurs de concevoir des parcours de formation, de les mettre en œuvre - notamment dans le cadre des écoles académiques de la formation continue (EAFC) et des opérateurs chargés de la formation des enseignants -, et de suivre l’acquisition des compétences en associant les apprenants ; 
* **Éléa** pour permettre aux enseignants de concevoir des parcours pédagogiques, de les mettre en œuvre avec leurs élèves et de suivre leur progression en les y associant ;
* le **« Réseau des concepteurs »** comme espace offrant à la communauté des concepteurs (enseignants et/ou formateurs) un service de scénarisation et de partage de parcours d’apprentissage, co-construits et maintenus dans le temps.

L’écosystème de création et de mutualisation de ressources est composé de :

* **Apps.education.fr** qui propose les outils essentiels du quotidien à l’ensemble des agents du ministère en charge de l’éducation ;
* la **Forge des communs numériques éducatifs**[^7] met à disposition des enseignants une forge GitLab en gouvernance partagée entre le ministère et la communauté de ses utilisateurs ;
* **Capytale**[^8], un service permettant la création et le partage d’activités de codage par les enseignants et de les mettre en œuvre avec leurs élèves.



## 3.4. Ressources numériques éducatives

Les ressources numériques éducatives désignent tout contenu et outil au format numérique, au bénéfice de l’enseignement et de l’apprentissage. Elles s’adressent spécifiquement aux enseignants et aux élèves, pour un usage en classe et/ou hors la classe. Elles doivent répondre aux orientations pédagogiques et aux prescriptions juridiques et techniques du ministère en charge de l’éducation.

!!!note "Doctrine"
	Les ressources numériques éducatives sont choisies par les enseignants en vertu de la liberté pédagogique dans le respect des réglementations en vigueur. Ce choix fait l’objet d’un échange *a minima* en Conseil d’école dans le premier degré, et s’inscrit dans la politique documentaire de l’établissement scolaire au second degré. Lorsqu’une ressource numérique implique un traitement de données à caractère personnel, ce choix se fait sous réserve de l'accord du responsable de traitement. L'utilisation d'une ressource numérique éducative peut cependant être déterminée par la réglementation (cas de Pix notamment[^9]).  
	Ces ressources s’insèrent dans les environnements numériques des usagers permettant d’assurer une simplicité de présentation, de navigation et une sécurité des accès.

Les ressources numériques sont fournies par des acteurs privés ou publics, elles peuvent être mises gratuitement à disposition par l’État ou par les collectivités territoriales, ou être achetées par l’établissement ou l’école. Leur utilisation peut relever d'une licence libre ou propriétaire. Suivant les modèles commerciaux des fournisseurs de ressources, la licence propriétaire implique parfois une durée.

Un établissement ou une école peut mettre à disposition de ses enseignants ou élèves des ressources payantes en acquérant des licences d’utilisation directement auprès des fournisseurs privés. Il existe également des marchés publics opérés par l’État ou les collectivités territoriales qui permettent l’achat de licences au bénéfice du plus grand nombre. Dans tous les cas, les ressources numériques éducatives doivent répondre aux orientations pédagogiques et aux prescriptions juridiques et techniques du ministère en charge de l’éducation. Le respect des normes et standards décrits dans le [référentiel d’interopérabilité](https://doctrine-technique-numerique.forge.apps.education.fr/interoperabilite/) permet l’interopérabilité entre les services ainsi que la portabilité des données.


[^6]: Outils de gestion du SI des collèges et des lycées, ENT ou autres services spécifiques à un type d’enseignement, à un environnement local particulier.

[^7]: La forge des communs numériques est accessible sur [https://apps.education.fr/](https://apps.education.fr/).

[^8]: Capytale est un commun numérique porté par l’académie de Paris.

[^9]: Arrêté du 30 août 2019 relatif à la certification Pix des compétences numériques définies par le cadre de référence des compétences numériques mentionné à l'article D. 121-1 du code de l'éducation.