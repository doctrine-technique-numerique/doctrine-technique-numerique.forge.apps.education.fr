# 4. Services socles nationaux

L’État met à disposition des services « socles » nationaux assurant un certain nombre de fonctions utiles voire nécessaires au bon fonctionnement d’ensemble des solutions numériques utilisées dans l’éducation.

Il s’agit notamment des fonctions d’authentification, de circulation des données d’organisation pédagogique, de gestion des accès aux ressources et de suivi de la fréquentation. Ces fonctions relèvent chacune d’un service national de référence, qui permet de garantir à la fois la bonne circulation et la souveraineté des données. Le raccordement à ces services socles, obligatoire mais non exclusif, est un critère essentiel de conformité aux exigences d’interopérabilité, de sécurité et de numérique responsable.

**Les mises à disposition complémentaires de données, entre acteurs habilités, selon des logiques d’exposition de données par API seront encadrées techniquement par la documentation de standards partagés (décrits dans le référentiel d’interopérabilité) et juridiquement par des contrats définissant les engagements respectifs.**

## 4.1. ÉduConnect pour l’identification et l’authentification des élèves et de leurs responsables

ÉduConnect est un dispositif fonctionnel et technique permettant :

* d’identifier les utilisateurs. ÉduConnect attribue un profil fonctionnel et propage auprès de chaque catégorie de service les informations d’identité nécessaires et suffisantes permettant l’accès de l’utilisateur authentifié ; 
* d’authentifier les utilisateurs. Selon le cas d’usage, ÉduConnect vérifie le couple identifiant / mot de passe ou délègue l’authentification à FranceConnect ; 
* de gérer les comptes utilisateurs. ÉduConnect permet de gérer l’attribution (par distribution ou auto-enrôlement), l’activation, la modification (par l’utilisateur ou par un administrateur), le dépannage (par l’utilisateur ou par une assistance) ; 
* de proposer à l’utilisateur l’accès à des services numériques sans réauthentification. 

Ce guichet permet de couvrir les catégories suivantes d'usagers du système d'information de l'Éducation nationale :

* les élèves des 1<sup>er</sup> et 2<sup>d</sup> degrés immatriculés (ayant un INE) scolarisés dans un établissement public (ou dans un établissement privé sous contrat sur la base du volontariat) ; 
* les responsables des élèves : les représentants légaux de ces élèves ; 
* les personnes en charge de ces élèves.

!!!note "Doctrine"
	Le service ÉduConnect assure l’identification des élèves et de leurs représentants légaux et leur authentification, pour l’accès à l’ensemble des services numériques pour l’éducation dans les 1<sup>er</sup> et 2<sup>d</sup> degrés. Le guichet est articulé avec FranceConnect, afin de permettre aux responsables d’élèves de fédérer leur identité entre les différents portails de services concernés.

Les principes du ministère pour la délégation de l’authentification des services numériques sont définis par catégorie de service, et décrivent les données fournies et la procédure de raccordement aux environnements de test et à l’environnement de production. Les **contrats d’interface ÉduConnect** sont référencés dans le référentiel d’interopérabilité.

## 4.2. Guichets agents pour l’identification et l’authentification des agents

!!! example ""
	[En cours]

Les guichets Agents désignent le système d’authentification des personnels de l’éducation nationale, basé sur les annuaires LDAP académiques des agents (AAA) et sur l’annuaire de l’administration centrale.

*NB : ces annuaires LDPA académiques peuvent référencer aussi des identifiants de personnels hors ministère en charge de l’éducation ayant accès aux applications du ministère (ex. : utilisateurs dans les mairies de l’application BE1D2/module-mairie pour le suivi des inscriptions d’élèves dans le 1<sup>er</sup> degré, agents des établissements maritimes…).*

Les guichets Agents disponibles sont accessibles via les hubs de fédération du Ministère :

* Le Hub national Agent permet l’authentification des agents présents dans les annuaires du Ministère pour l’accès aux services du Ministère et pour des services proposés par ses opérateurs et ses partenaires.
* Le Hub Partenaires permet l’accès authentifié aux services du Ministère pour les agents d’autres ministères disposant d’un guichet d’authentification.
* Le Hub FER permet l’accès authentifié aux services du Ministère pour les agents d’institutions de l’enseignement supérieur ou des entités fédérées avec la Fédération Éducation Recherche de Renater
* Le Hub AgentConnect permet l’accès authentifié aux services du Ministère pour les agents des ministères dont le guichet est fédéré avec AgentConnect
  
Le contrôle d’accès (autorisation d’accès) aux services du Ministère pour l’ensemble des agents est régis par les politiques d’habilitations du Ministère. L’inclusion d’un service dans le bouquet de services proposé aux agents est sujette à un processus de validation de la part du ministère.

!!!note "Doctrine"
	Les guichets Agents assurent l’identification et l’authentification des personnels de l’éducation nationale et des autres personnels présents dans les annuaires d’identité du ministère pour l’accès à l’ensemble des services numériques pour l’éducation mis à leur disposition par le ministère, par l’un de ses opérateurs ou par des partenaires.

## 4.3. Scope pour la circulation des données d’organisation pédagogique dans le second degré

Les données d’organisation pédagogique gérées dans les solutions choisies par les établissements (dont les solutions d’emplois du temps) sont indispensables pour l’ensemble des services numériques pour l’éducation qui outillent les pratiques pédagogiques et les missions éducatives, ainsi que pour certaines interactions avec le système d’information du ministère et des collectivités territoriales. Pour que les utilisateurs de ces services[^10] puissent retrouver la finesse de description initiale de la solution source, sans procéder à des ressaisies dans la solution destinataire, les données d’organisation pédagogique issues ou alimentant les solutions doivent circuler de manière fluide au sein de l'écosystème constitué de l'État et de ses partenaires habilités, dans le respect de contrats d’interface définis entre les acteurs.

!!!note "Doctrine"
	Tout service dit d’emploi du temps, ou offrant des fonctionnalités de modification de l’organisation pédagogique[^11] au responsable de traitement (chef d’établissement ou DASEN dans le premier degré) doit permettre la circulation des données d’organisation pédagogique (dont l’emploi de temps) dans un cadre maitrisé et documenté, vers des acteurs habilités.  
	Pour cela, l’État met à disposition « Scope » – service outillant la circulation de l’organisation pédagogique de l’établissement – s’appuyant sur Siècle VE.[^12].

En entrée, tout acteur fournissant une solution dite d’emploi du temps, ou offrant des fonctionnalités de modification de l’organisation pédagogique, et mise en œuvre en accord avec l’établissement se raccorde à Scope.

En sortie, Scope alimente les solutions consommatrices d’organisation pédagogique habilitées, dans le respect des contrats d’interface définis entre les acteurs.

Les acteurs raccordés à Scope mettent en œuvre les éléments définis au contrat d’interface pour assurer l’exactitude de la remontée des données.

**Les contrats d’interface Scope** sont référencés au sein du [référentiel d’interopérabilité des services numériques pour l’éducation](https://doctrine-technique-numerique.forge.apps.education.fr/interoperabilite/).

## 4.4. GAR pour l’accès aux ressources numériques éducatives

Le GAR est le dispositif technique et juridique permettant, d’une part, la gestion simplifiée de l’accès aux ressources numériques au sein d’une école ou d’un établissement et, d’autre part, la minimisation des données communiquées aux distributeurs et éditeurs de ressources numériques éducatives. Il porte les habilitations entre fournisseurs d’identité, fournisseurs de données et fournisseurs de ressources numériques pour l’éducation. 

Le GAR a été mis en place dans un contexte de multiplication des ressources numériques pour l’école nécessitant, pour leurs fonctionnalités interactives, le recours aux données à caractère personnel. Le ministère chargé de l’éducation nationale a créé le traitement de données à caractère personnel GAR afin de permettre l'accès des élèves et des enseignants à leurs ressources numériques et services associés via un espace numérique de travail (ENT), un médiacentre ou un équipement mobile. 

Il est articulé avec les dispositifs d’authentification ministériels (ÉduConnect et le guichet Agents) et les portails de services numériques territoriaux (actuellement, projets de type ENT).

!!!note "Doctrine"
	Pour toutes les solutions et ressources numériques qui permettent aux enseignants d’outiller leurs pratiques pédagogiques avec leurs élèves, la gestion des autorisations d’accès – en ligne ou via des « applications natives » – se fait via le Gestionnaire d’Accès aux Ressources (GAR), mis à disposition par le ministère en charge de l’éducation.  
	Le GAR gère les autorisations d’accès pour toutes les ressources numériques, présentées dans les médiacentres des ENT ou autres projets territoriaux (via ÉduGAR).

Les finalités du traitement GAR[^13] sont les suivantes :

* la validation par le ministère des demandes de données strictement nécessaires au fonctionnement du service par les fournisseurs de ressources ;
* la transmission aux fournisseurs de ressources des données strictement nécessaires aux accès des élèves et des enseignants à ces ressources et à leur utilisation en fonction des droits qui leur sont ouverts ;
* l’hébergement des données produites au sein des ressources par les utilisateurs ;
* le suivi statistique des accès aux ressources numériques pour l’analyse de la qualité de service délivrée et le suivi de l’utilisation de ces ressources dans les limites prévues par le contrat d'adhésion au GAR.

Le ministère est le seul responsable de traitement pour tous les accès de tous les utilisateurs de tous les établissements ou écoles. Aucune fiche registre supplémentaire n’est donc nécessaire. Les fournisseurs de ressources sont des sous-traitants du ministère au sens du RGPD, y compris pour les données produites par l’utilisateur dans le contexte de la ressource (hébergement, durée de conservation, etc.).

Le fournisseur de ressources responsable éditorial signe un contrat d’adhésion au GAR en tant que sous-traitant, en application de l’article 28 du RGPD. 

Le processus d’accrochage au GAR repose sur un référentiel technique fonctionnel et de sécurité défini au sein du référentiel d’interopérabilité des services numériques pour l’éducation. Le RTFS fournit notamment les contrats d’interface GAR.

## 4.5. Un DNMA pour le pilotage des services numériques pour l'éducation

Afin d’améliorer l’aide à la décision et le pilotage des projets de services numériques dans l’éducation, la définition d’indicateurs partagés normalisés, pertinents et comparables est nécessaire.

Il y a un besoin partagé des porteurs de projet de rendre compte collectivement des utilisations des services numériques pour l’éducation dans un dispositif commun à la fois fiable, transparent et homogène, dans un contexte où les projets de services numériques sont de périmètre variable, avec des acteurs industriels et plateformes différents.

!!!note "Doctrine"
	Tout service numérique pour l’éducation doit permettre aux responsables en charge de son pilotage de disposer d’indicateurs d’utilisation et de fréquentation, comparables dans le temps, avec d’autres services et entre projets.
	
	Pour cela, le ministère en charge de l’éducation met à disposition un dispositif national de mesure d’audience (DNMA) qui permet d’outiller les acteurs sur deux axes :

	* un référentiel de marquage national, décrivant les indicateurs principaux (nombre de visites, visiteurs, définis selon la norme commune ACPM[^14]) et la manière d’implémenter les marqueurs dans les services ;
	* une solution de collecte des marqueurs externes aujourd’hui opérée par le ministère pour le périmètre des ENT.

Le DNMA des ENT mis à disposition par le ministère repose sur une solution de « marquage » externe conforme à un cahier des charges partagé qui définit le contenu et la structure des marqueurs. Ces éléments sont fournis dans le référentiel d’interopérabilité des services numériques pour l’éducation.

Cette solution est conforme aux exigences du RGPD (formalités réalisées au niveau national, exigences de sécurité et de confidentialité des données remontées par les marqueurs externes).

Les responsables de traitement des services numériques pour l’éducation doivent inscrire la finalité statistique du DNMA sur la fiche registre correspondant au traitement concerné et veiller à ce que les utilisateurs soient informés de la mise en œuvre de ces traceurs.

Si la mise à disposition d’un cadre de marquage commun à l’ensemble des services numériques pour l’éducation est envisagée à horizon 2024-2025, le déploiement de la solution de collecte des marqueurs est aujourd’hui planifié par le ministère sur le périmètre des projets ENT. 

## 4.6	Siècle-GFE pour calculer les droits constatés

!!!note "Doctrine"  
	Le service Siècle-GFE assure le calcul réglementaire des droits constatés. Le calculateur des droits constatés garantit l’égalité de traitement en appliquant les exigences réglementaires quelle que soit la solution utilisée pour la gestion de la facturation en établissement.

Les processus métier gérés par le calculateur sont les suivants :
 
* le calcul des droits constatés (en tenant compte des bourses, primes, aides nationales et locales) ;
* la simulation des droits constatés ;
* l’édition du bordereau des droits constatés ;
* l’envoi du flux des droits constatés à Op@le.

Siècle-GFE sera mis en service au premier semestre 2025.


[^10]: Utilisateurs concernés par les activités des processus listés au chapitre 2 « Notions et processus métiers » (figure 4)

[^11]: Voir paragraphe 2.2

[^12]: SIECLE Vie de l’établissement

[^13]: [Arrêté GAR du 18/12/2017](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000036249969/)

[^14]: Alliance pour les chiffres de la presse et des médias.