# 1. Fondements et orientations

Dans le respect des principes de liberté pédagogique, d’autonomie des établissements et de répartition des compétences entre État et collectivités territoriales, la doctrine technique du numérique pour l’éducation est au service d’un numérique responsable et souverain. Elle vise à la protection des données personnelles pour permettre leur valorisation dans un cadre qui garantit la gratuité pour les usagers, l’égalité territoriale, l’intégration des contraintes environnementales et une insertion pertinente des activités numériques dans la vie de l’établissement/école et de la classe dans un cadre sécurisé.

Pour cela, ce document fournit les règles, exigences et outils nécessaires pour faciliter l’interopérabilité, la sécurité et la circulation des données entre les acteurs publics et privés, avec un cadre d’architecture des services. Ces exigences et règles ont vocation à devenir, pour certaines d’entre elles, opposables juridiquement (cf. [paragraphe 1.2.3. Démarche d’opposabilité]()). 

Les travaux d’interopérabilité pour la mise en œuvre de ce cadre sont assurés par les fournisseurs de services et porteurs de projet (industriels, État, opérateurs, collectivités territoriales…). Toutes les modifications conduisant à de nouvelles adaptations (hors modification légale qui s'impose dès publication) seront intégrées en conscience des délais de mise en œuvre pour les acteurs concernés. En raison de la spécificité et de l’autonomie de l’enseignement privé, ces exigences sont formulées pour les acteurs intervenant dans le périmètre de l’enseignement public[^1].

## 1.1. Fondements de la doctrine technique

### 1.1.1. Stratégie numérique 2023-2027

La [vision stratégique 2023-2027 du numérique pour l’éducation](https://www.education.gouv.fr/strategienumerique) pose notamment l’ambition de soutenir la communauté éducative en permettant aux fournisseurs de services de contribuer à une offre numérique raisonnée, pérenne et inclusive. Pour cela, elle prévoit d’» organiser l’offre de services numériques pour l’éducation selon une logique de plateforme interopérable ».

Selon le principe de « l’État plateforme », l’État doit fournir un cadre aux services numériques pour l’éducation proposés aux usagers – élèves, parents, professeurs, personnels d’encadrement, administratif et technique – pour organiser et promouvoir une offre simple, cohérente, pertinente et durable au service des élèves, de leur famille, de la communauté pédagogique, éducative et des personnels d’encadrement sur l’ensemble du territoire national.

Cette stratégie se décline par la mise à disposition par l’État de « services socles » et de référentiels de données, mais aussi par la fixation d’un ensemble de règles communes (en matière notamment d’urbanisation, d’interopérabilité, de protection des données, de sécurité, d’accessibilité ou encore d’éthique) régissant l’offre de services du numérique pour l’éducation.

Les utilisateurs gagnent alors en qualité : une plus grande cohérence des interfaces, une suppression des ressaisies, une aisance à l’usage, une protection et une sécurité des données et des services.

En stabilisant et en clarifiant les règles du jeu, l’État permet en outre à chaque acteur – administration, collectivités, fournisseurs services numériques pour l’éducation – d’apporter ses services dans un contexte cohérent, national ou territorial, et sans rupture de parcours pour l’utilisateur final, dans le respect des valeurs et du cadre définis et de la libre concurrence du marché du numérique éducatif.

Le service public de l’éducation y gagne ainsi en agilité, permettant aux usagers et aux sociétés de la filière industrielle du numérique éducatif de bénéficier de services innovants, mais aussi en souveraineté en excluant toute solution non respectueuse des règles édictées, notamment en matière d’éthique et de protection des données.

### 1.1.2. Périmètre couvert par la doctrine technique

La doctrine technique du numérique pour l’éducation fournit le cadre au sein duquel les acteurs publics et privés peuvent proposer des services numériques pour l’éducation pour les professionnels et les usagers du système éducatif.

Le schéma ci-après fournit une vue synthétique des principaux composants de ce cadre.

![Figure 1 : La doctrine technique au service du numérique pour l'éducation](Figures/Figure1.1.2.png "Figure 1 : La doctrine technique au service du numérique pour l'éducation")
*Figure 1 : La doctrine technique au service du numérique pour l'éducation*

Dans ce schéma, la logique de plateforme se décline de façon opérationnelle à plusieurs niveaux.

**La définition de règles et bonnes pratiques**, en matière de sécurité, d’interopérabilité et de numérique responsable, indispensables pour faciliter la protection, le partage et l’échange des données d’éducation en toute confiance et en cohérence avec les normes et standards internationaux. Les principes généraux de ces règles et bonnes pratiques sont déclinés dans les trois référentiels interopérabilité, sécurité et numérique responsable. Leur déclinaison technique sera également rendue publique au travers de documents de référence publiés sur le site du ministère. Le respect des exigences décrites dans ces documents s’appuiera à terme sur la publication d’un texte de loi. 

**La mise à disposition de gisements de données de référence** qui doivent pouvoir alimenter les services numériques. L’État doit garantir la qualité de ces données (exactitude, fraîcheur, etc.), faciliter leur exploitation (normalisation, définition des contraintes d’accès) par les acteurs et s’assurer du respect des différentes contraintes inhérentes au domaine de l’éducation (confidentialité des données, respect de la vie privée et droit à l’oubli, propriété intellectuelle, etc.). 

**Le déploiement de « services socles » nationaux**, indispensables au bon fonctionnement du numérique éducatif. L’État intervient ainsi en fournissant un service public permettant d’outiller les fonctions suivantes :

* l’identification/authentification
      * des élèves et de leurs responsables (ÉduConnect),
      * des agents de l’Éducation nationale (guichets-Agents) ;
* l’accès simple et sécurisé aux ressources numériques pour l’École choisies par l’équipe pédagogique au sein de l’établissement ou de l’école 
      * dispositif GAR – gestionnaire d’accès aux ressources –, permettant l’interopérabilité entre les portails de services territoriaux délivrant un « médiacentre » et les fournisseurs des ressources, ou par une entité territoriale,
      * dispositif ÉduGAR délivrant un médiacentre pour l’accès aux ressources en l’absence de portail de service territorial ;
* la circulation des données d’organisation pédagogique de l’établissement (Scope – service outillant la circulation de l’organisation pédagogique de l’établissement – porté par le système d’information ministériel Siècle Vie de l’établissement) ;
* la mesure d’audience des services numériques, pour le pilotage des politiques publiques nationales et territoriales, avec des indicateurs normés, comparables et pérennes, dont le DNMA – dispositif national de mesure d’audience des ENT.

**La mise à disposition de services et portails numériques d’État** permettant d’assurer :

* les démarches en ligne correspondant aux formalités administratives obligatoires, en particulier pour les familles (portail Scolarité services) ;
* la résilience du système éducatif et l’égalité d’accès aux services numériques pour l’éducation pour tous (bouquet de communs numériques de l’État).



### 1.1.3. Services numériques pour l'éducation : définition et typologie

#### 1.1.3.1. Vue d’ensemble

![Figure 2 : Les services numériques éducatifs (SNE)](Figures/Figure1.1.3.png "Figure 2 : Les services numériques éducatifs (SNE)")
*Figure 2 : Les services numériques éducatifs (SNE)*

#### 1.1.3.2. Vue fonctionnelle des services numériques éducatifs

> *[En cours] Ce paragraphe vise à préciser les fonctions de chacun des blocs identifiés dans la partie « pour quelles missions ? » de la figure 2.*

La figure 2 *au paragraphe 1.1.3.1* fournit une vue d’ensemble des services numériques pour l’éducation. Elle sera complétée – dans une version ultérieure de la doctrine technique du numérique pour l’éducation – par une vue fonctionnelle en cours d’élaboration dans le cadre des travaux du paragraphe 1.2.1 dédiés à la vue urbanisée des services numériques pour l’éducation.

Chacun des blocs fonctionnels de la figure 2 pourra ainsi être précisé : définition, fonctionnalités numériques principales, domaines de compétences.

### 1.1.4. Positionnement et organisation du document doctrine technique

La présente doctrine technique du numérique pour l’éducation se concentre sur le cadre dans lequel doivent s’inscrire l’ensemble des « services numériques pour l’éducation » (SNE – voir glossaire). Elle fournit les documents de référence pour permettre des échanges de données partagés, sécurisés et pérennes, ainsi que pour assurer le niveau de qualité de service nécessaire à la mise en œuvre de la continuité pédagogique, dans ses différentes modalités (en présence, à distance ou en hybride).

Ce document s’adresse d’abord aux porteurs de projets de services numériques pour l’éducation, qu’ils en assurent la maîtrise d’ouvrage ou la maîtrise d’œuvre. Il vise à ce que chaque acteur puisse apporter des services à valeur ajoutée avec les mêmes règles, et à ce que chaque utilisateur bénéficie de ces services dans le cadre de confiance fixé par le ministère.

Le document décrit pour cela les processus métier concernés, les données échangées, les services socles fournis par l’État, les éléments d’interopérabilité liés à ces services ainsi que les exigences pour tous les projets et acteurs territoriaux ou nationaux, publics ou privés.

Son utilisation facilite l’intégration des systèmes pour **fournir une offre de services finale centrée sur les besoins de l’usager**, et pour permettre aux différents acteurs d’échanger des données simplement sans développements informatiques complexes ou coûteux.

Clé de voûte d’un cadre évolutif, ce document constitue également un instrument de dialogue entre l’Éducation nationale, ses partenaires et les acteurs de la filière industrielle. À cette fin, il s’appuie sur des méthodes collaboratives, qui seront progressivement outillées pour faciliter les échanges et l’innovation au sein de la communauté des acteurs du numérique éducatif.

La doctrine technique du numérique pour l’éducation s’articule en sept chapitres, décrits ci-après.


1. **Fondements et orientations**  
   *Ce volet décrit la raison d’être, les partis pris et perspectives de la doctrine technique du numérique pour l'éducation. Il fournit également une cible et trajectoire annuelle, en déclinaison de la vision stratégique du numérique pour l’éducation.*
2. **Dictionnaire des notions et processus métiers**  
   *Ce volet décrit les notions métiers afin de partager les mêmes objets et clarifier les finalités.*
3. **Services et portails de services numériques**  
   *Ce volet décrit les services et portails nationaux, territoriaux ou de ressources numériques éducatives.*
4. **Services socles**  
   *Ce volet décrit les services numériques relevant d’un service national de référence, permettant de garantir la bonne circulation des données dans un cadre de confiance.*
5. **Données et systèmes d’information d’éducation**  
   *Ce volet décrit les différents types de données, leur organisation dans les SI d’éducation et leurs caractéristiques principales.*
6. **Règles et cadres de référence**  
   *Ce volet décrit les règles et cadres d'interopérabilité, de sécurité et de numérique responsable pris en compte, pour assurer la simplification des parcours usagers, la protection et la valorisation des données.*
7. **Bonnes pratiques**  
   *Ce volet est consacré aux bonnes pratiques qui peuvent être mises en commun pour accompagner la mise en œuvre de la doctrine.*

Chacun des services numériques pour l’éducation décrits dans les chapitres 3 et 4 s’inscrit dans le cadre de la stratégie du numérique pour l’éducation 2023-2027 et, pour les projets relevant du ministère, se décline dans une feuille de route détaillant sa mise à disposition et sa trajectoire.

Le présent document constitue la **deuxième version** de la doctrine technique du numérique pour l’éducation.

Une priorisation des axes couverts par cette version conduit à traiter certains sujets et à en écarter d’autres dont le traitement est reporté à une version ultérieure. Le document présente en conséquence des chapitres ou paragraphes annotés « en cours » ou « à venir ».

Pour cette deuxième version, l’effort s’est centré sur les données d’éducation pour en permettre l’utilisation et l’ouverture dans un cadre maîtrisé et au service des usagers, ainsi que sur l’initialisation de trois référentiels au cœur de la doctrine technique, consacrés aux exigences d’interopérabilité, de sécurité et de numérique responsable.

## 1.2. Orientations

### 1.2.1. Urbanisation et architecture cible du numérique éducatif

L’architecture cible du numérique éducatif est représentée dans un schéma décrivant une vue générale urbanisée des services numériques pour l’éducation et évoluera au fil des versions vers un ou des schémas d'architecture.

Ce schéma fait l’objet d’un travail concerté entre l’État et les collectivités territoriales, au sein des instances de pilotage du chantier Doctrine technique du numérique pour l’éducation, et évoluera au fil des versions annuelles de la doctrine.

Ce schéma s’appuie sur les composants décrits au *paragraphe 1.1.3* et ses déclinaisons décriront la cible en termes d’urbanisation des services.

![Figure 3 : Vue générale urbanisée des services numériques pour l’éducation](Figures/Figure1.2.1.0.png "Figure 3 : Vue générale urbanisée des services numériques pour l’éducation")
*Figure 3 : Vue générale urbanisée des services numériques pour l’éducation*

Des principes généraux d’urbanisation pourront compléter ce schéma (exemple : volet juridique des projets de services, données de référence ou standards assortis d’une gouvernance clairement définie, stratégie de conception et d'articulation des services applicatifs pour l’écoconception et l’accessibilité, etc.).

### 1.2.2. Macro-planning et priorités pour la feuille de route 2023-2024

![Figure 4 : Macro-planning des chantiers pour la doctrine technique](Figures/Figure1.2.2.png "Figure 4 : Macro-planning des chantiers pour la doctrine technique")
*Figure 4 : Macro-planning des chantiers pour la doctrine technique*

Si la version 2023 de la doctrine a permis de cadrer la démarche d’ensemble, la présente version vise à préciser les exigences relatives aux données d’éducation, pour en faciliter l’exploitation, la protection et la mise à disposition pour servir les activités pédagogiques et missions éducatives. 

Les priorités du programme de travail associé à la doctrine technique du numérique pour l’éducation pour l’année 2023-2024 ont porté sur les éléments suivants :

* l’initialisation du référentiel d’interopérabilité définissant les premières exigences d’interopérabilité, de façon à contribuer à la bonne circulation des données nécessaires à l’ensemble des services numériques pour l’éducation ;
* l’initialisation du cadre général de sécurité et du référentiel du numérique responsable ;
* la mise en place des actions pour l’opposabilité des règles d’interopérabilité, de sécurité et de numérique responsable ;
* la description des schémas d’architecture fonctionnelle et d’urbanisation cible du domaine numérique éducatif ;
* l’identification d’outils opérationnels pour accompagner la doctrine technique ; 
* l’accompagnement des porteurs de projet territoriaux pour inscrire leurs projets dans une logique de plateforme. 

Pour porter ces sujets, le ministère coordonne les travaux par le biais des comités de concertation mis en place en phase de cadrage depuis le premier semestre 2022. Ces comités restent des instances de travail avec les différents partenaires pour la définition des orientations et la mise à jour annuelle du document. Les délais de mise en conformité seront définis dans le cadre de la démarche d'opposabilité.

### 1.2.3. Démarche d’opposabilité

Pour que la doctrine technique soit un véritable outil d’engagement des acteurs du numérique éducatif aux côtés des pouvoirs publics, certaines de ces règles seront rendues juridiquement opposables par voie de loi.

Les premières étapes de la démarche d'opposabilité ont permis d’effectuer un bilan partagé de la situation concernant les référentiels et services socles déjà existants, les contrats et conventions, ainsi que les textes réglementaires du périmètre.

Un texte de loi est en cours de préparation, afin de construire un cadre légal commun pour les services numériques pour l’éducation. 

La référence à la doctrine technique dans les cahiers des charges des marchés publics passés par les acteurs territoriaux permet de rendre contractuellement opposables les règles qu’elle contient.

### 1.2.4. Démarche de sécurité des services numériques pour l’éducation

La doctrine pose un cadre général de sécurité numérique qui permet d’identifier les actions requises afin de garantir la sécurité des services numériques pour l’éducation en lien avec les autres acteurs compétents et de manière cohérente. Elle rappelle l’importance d’une collaboration et d’un alignement des actions relatives à la sécurité à travers l’ensemble des acteurs. Elle rappelle également l’importance d’installer la sécurité dans l’ensemble de son cycle de vie à travers des actions de sensibilisation, de protection, de défense et de réaction aux attaques ayant porté atteinte au numérique.

### 1.2.4. Outils opérationnels de la doctrine technique

Des outils seront progressivement mis à disposition des acteurs concernés pour rendre opérationnels les éléments de doctrine fournis dans les versions successives du présent document. Dans un premier temps sont fournis des éléments documentaires :

1. Principaux concepts métier et description des services numériques pour l’éducation (permettant de définir précisément les finalités des traitements de données dans les registres de traitement mis en place pour le RGPD), glossaire des acronymes et termes utilisés dans le domaine des services numériques pour l’éducation ;
2. kits d’accompagnement des porteurs de projet :  
      * kits « Informatique et libertés » pour projets de services numériques, avec modèles de conventions, modèle de fiche registre pour les traitements établissement (pour les projets ENT, cf. kit de conventionnement « Informatique et libertés » en annexe du SDET en vigueur) ;
      * kit « de conservation et d’archivage des données ENT ».

Il est envisagé de compléter ces outils par des dispositifs permettant d’accompagner les acteurs d’une part dans la mise en conformité et d’autre part dans l’évolution du cadre documentaire lui-même. Ces outils devront permettre la remontée des besoins, leur hiérarchisation et la prise de décision pour leur traduction en actions concrètes.



[^1]: Ou écoles / établissements privés sous contrat qui le souhaitent

[^2]: [Schéma directeur des ENT](https://eduscol.education.fr/1559/schema-directeur-des-ent-sdet-version-en-vigueur "Schéma directeur des ENT")
