# Journal des modifications 

Ce journal des modifications ne concerne que le document principal de la doctrine technique du numérique pour l'éducation. Pour les autres documents, voir les pages correspondantes.

## Version 2024

### Juillet 2024
02/07/2024 - Publication de la version Juillet 2024 après prise en compte des retours de la consultation publique.

### Appel à commentaires
du 25 mars 2024 au 3 juin 2024

*ajouts par rapport à la V1 (2023) :*

* Chapitre 1.2.1. : ajout du schéma d'architecture SI
* Chapitre 3 : clarification des communs numériques dans la partie 3.3.
* Ajout du chapitre 5 : Données et systèmes d’information (SI) d’éducation
* Coupes dans le chapitre 6 : les informations sont reportées dans les nouveaux documents
* Ajout du chapitre 7 : Bonnes pratiques
* Le glossaire est retiré et devient un document à part entière

## Version 1 (2023)

### Version 1.1
27/06/2023 - Correction de la numérotation dans la *Partie 6 - Règles et cadres de référence*

* [Version en PDF, disponible sur Éduscol](https://eduscol.education.fr/document/49067/download?attachment)
* Grilles de synthèse de l'appel à commentaires 2023
    * [Partie 1](https://eduscol.education.fr/document/49580/download)
    * [Partie 2](https://eduscol.education.fr/document/49583/download)

### Version 1.0
23/05/2023 - Publication d'une version 1 du référentiel après prise en compte des retours de la consultation publique.