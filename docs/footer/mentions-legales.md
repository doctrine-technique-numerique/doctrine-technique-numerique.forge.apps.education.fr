# Mentions légales


Ce site (https://doctrine-technique-numerique.forge.apps.education.fr/) a été mis en ligne en 2024

## À propos du site

### Objectif et qualité des contenus

Les usages des services numériques éducatifs dans les 1<sup>er</sup> et 2<sup>d</sup>  sont toujours plus nombreux, non seulement à des fins administratives et de suivi de la scolarité, mais aussi au bénéfice d’activités pédagogiques et de mise en œuvre des missions éducatives. Le corollaire est logiquement une augmentation conséquente de la fréquentation des services, qui nécessite à la fois une parfaite maîtrise des données ainsi qu’un écosystème ouvert et interopérable. Avec des services numériques au cœur de la continuité pédagogique, les attentes et les besoins associés sont nombreux sur la simplification des parcours usagers, la disponibilité des services, le cadre de confiance qui assure la sécurité des données et leur échange entre des acteurs habilités.

### Objectifs et positionnement de la doctrine technique du numérique pour l’éducation

Afin de mettre à disposition des usagers un ensemble lisible et structuré de services numériques accessibles simplement et interopérables entre eux, le numérique pour l’éducation doit s’organiser selon une logique de « plateforme ». Pour cela, un cadre d’architecture et des règles communes, facilitant la circulation des données entre les acteurs publics et privés, sont rassemblés dans un corpus documentaire : la doctrine technique du numérique pour l’éducation.

La doctrine technique exprime les exigences attendues pour les fournisseurs de services numériques éducatifs et renvoie à 3 référentiels dédiés aux exigences d’interopérabilité, de sécurité, de numérique responsable et qui ont vocation à devenir opposables par voie législative. Cet ensemble documentaire est issu d’un travail mené en collaboration avec les parties prenantes concernées.

## Informations éditoriales

### Éditeur
Ministère de l’Éducation nationale et de la Jeunesse  
Direction générale de l'enseignement scolaire - Bureau de la diffusion et de l'information  
107 rue de Grenelle, 75007 Paris

### Directeur de la publication
Audran le Baron  
Directeur de la direction du numérique pour l’éducation

### Rédaction et maîtrise d’ouvrage
Le bureau des services et outils numérique pour l’éducation (DNE SN1) assure la coordination du processus de production, de validation et d'accessibilité des documents.

## Informations conception, suivi technique, hébergement

### Hébergeur
* Nom : La Forge des communs numériques éducatifs 
* Lien site : https://forge.apps.education.fr/

### Conception graphique et gestion de contenu

Ce site utilise le générateur de site statique MkDocs avec le Package d'Extensions MkDocs DSFR développé par le Ministère Transition Écologique – DNUM.

## Propriété intellectuelle

Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées sur https://doctrine-technique-numerique.forge.apps.education.fr/.

Sauf mention de propriété intellectuelle détenue par des tiers, les contenus de ce site sont proposés sous [licence ouverte 2.0](https://github.com/etalab/licence-ouverte/blob/master/LO.md).

La mise en place de liens vers nos contenus et documents est cependant est à privilégier par rapport à la reproduction sur un site tiers. La mention explicite du site source https://doctrine-technique-numerique.forge.apps.education.fr/ est recommandée.
