# Plan du site

## [Accueil](../index.md)

## Doctrine technique
* [Préambule](../texte/0-preambule.md)
* [1/ Fondements et orientations](../texte/1-fondements-et-orientations.md)
* [2/ Dictionnaire des notions et processus métiers](../texte/2-dictionnaire-notions-et-processus%20metiers.md)
* [3/ Services et portails de services numériques](../texte/3-services-et-portails-de-services-numeriques.md)
* [4/ Services socles nationaux](../texte/4-services-socles-nationaux.md)
* [5/ Données et système d’information (SI) d’éducation](../texte/5-donnees-et-systeme-d-information-d-education.md)
* [6/ Règles et cadres de référence](../texte/6-regles-et-cadres-de-reference.md)
* [7/ Bonnes pratiques](../texte/7-bonnes-pratiques.md)

## Autres documents
* [Cadre général de sécurité des services numériques pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/securite/)
* [Référentiel d'interopérabilité des services numériques pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/interoperabilite/)
* [Référentiel du numérique responsable pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/numerique-responsable/)

## [Glossaire](https://doctrine-technique-numerique.forge.apps.education.fr/glossaire/)

## À propos
* [Licence](../LO.md)
* [Journal des modifications](../journal-des-modifications.md)