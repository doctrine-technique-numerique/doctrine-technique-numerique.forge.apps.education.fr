# 

Le ministère de l’éducation nationale publie une **nouvelle version (Juillet 2024) de la doctrine technique du numérique pour l’éducation**, afin de consolider le cadre d’architecture et de règles communes, visant à fournir aux usagers un ensemble lisible et structuré de services numériques pour l’éducation accessibles simplement et interopérables. 

## Pourquoi une doctrine technique du numérique pour l’éducation ?

L’utilisation des services numériques éducatifs dans les 1<sup>er</sup> et 2<sup>d</sup> degrés a connu une croissance accélérée depuis 2020. Les usages sont toujours plus nombreux, non seulement à des fins administratives et de suivi de la scolarité, mais aussi au bénéfice d’activités pédagogiques et de mise en œuvre des missions éducatives. Le corollaire est logiquement une augmentation conséquente de la fréquentation des services, qui nécessite à la fois une parfaite maîtrise des données ainsi qu’un écosystème ouvert et interopérable. Avec des services numériques au cœur de la continuité pédagogique, les attentes et les besoins associés sont nombreux sur la simplification des parcours usagers, la disponibilité des services, le cadre de confiance qui assure la sécurité des données et leur échange entre des acteurs habilités.

## Objectifs et positionnement de la doctrine technique du numérique pour l’éducation

Afin de mettre à disposition des usagers un ensemble lisible et structuré de services numériques accessibles simplement et interopérables entre eux, le numérique pour l’éducation doit s’organiser selon une logique de « plateforme ». Pour cela, un cadre d’architecture et des règles communes, facilitant la circulation des données entre les acteurs publics et privés, sont rassemblés dans un document : la doctrine technique du numérique pour l’éducation. Elle est publiée ici dans sa version Juillet 2024.

La doctrine technique exprime les exigences attendues pour les fournisseurs de services numériques éducatifs et renvoie à trois référentiels dédiés aux exigences d’interopérabilité, de sécurité, de numérique responsable et qui ont vocation à devenir opposables par voie législative. Cet ensemble documentaire est issu d’un travail mené en collaboration avec les parties prenantes concernées.

Le corpus documentaire comprend :

* La [doctrine technique du numérique pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/)
* Le [cadre général de sécurité des services numériques pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/securite/)
* Le [référentiel d'interopérabilité des services numériques pour l'éducaiton](https://doctrine-technique-numerique.forge.apps.education.fr/interoperabilite/)
* Le [référentiel du numérique responsable pour l'éducation](https://doctrine-technique-numerique.forge.apps.education.fr/numerique-responsable/)
* Le [glossaire](https://doctrine-technique-numerique.forge.apps.education.fr/glossaire/texte/glossaire/)

Le journal des modifications de chaque document liste les évolutions.

Une version PDF est [disponible sur Éduscol](https://eduscol.education.fr/3827/doctrine-technique-du-numerique-pour-l-education).