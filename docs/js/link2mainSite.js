const siteUrl = "https://doctrine-technique-numerique.forge.apps.education.fr/";

const headerElement = document.querySelector('.fr-header__service a');
const footerLogoElement = document.querySelector('.fr-footer__brand.fr-enlarge-link a')
headerElement.href=siteUrl;
footerLogoElement.href = siteUrl;